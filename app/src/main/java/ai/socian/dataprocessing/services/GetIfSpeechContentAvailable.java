package ai.socian.dataprocessing.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ai.socian.dataprocessing.model.SpeechContent;
import ai.socian.dataprocessing.util.CommonActivity;
import ai.socian.dataprocessing.util.Constant;
import ai.socian.dataprocessing.util.SharedPreferencesData;

/**
 * Created by tamzid on 1/28/18.
 */

public class GetIfSpeechContentAvailable extends BroadcastReceiver   {

    final public static String TAG = "Location";
    final public static String ONE_TIME = "onetime";
    SharedPreferencesData sharedPreferencesData;
    Context mcontext;
    List<SpeechContent> speechContentList = new ArrayList<SpeechContent>();




    @Override
    public void onReceive(final Context context, Intent intent) {
//        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "YOUR TAG");

        sharedPreferencesData = new SharedPreferencesData();
        //Acquire the lock
//        wl.acquire();

        //You can do the processing here.

        Bundle extras = intent.getExtras();

        StringBuilder msgStr = new StringBuilder();


        Log.i(TAG, "OnReceive called");

        if (extras != null && extras.getBoolean(ONE_TIME, Boolean.FALSE)) {

            //Make sure this intent has been sent by the one-time timer button.

            msgStr.append("One time Timer : ");

        }

        Format formatter = new SimpleDateFormat("hh:mm:ss a");

        msgStr.append(formatter.format(new Date()));


        System.out.println("Recevied_Time:::" + msgStr);


//        Utility.checkAndRequestPermissions((Activity) context);
// check if GPS enabled
//        gpsTracker = new GPSTracker(context);
////        gpsTracker.getLocation();
//
//        if (gpsTracker.getIsGPSTrackingEnabled())
//        {
//            latitude =gpsTracker.latitude;
//            longitude= gpsTracker.longitude;
//



        CommonActivity commonActivity = new CommonActivity();
        commonActivity.loadDataIfnotAvailable();
        commonActivity.points(Constant.point_speech);
        commonActivity.points(Constant.point_sentiment);



        //Release the lock
//        wl.release();
    }



    public void SetAlarm(Context context)

    {
        AlarmManager alarmManager=(AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, GetIfSpeechContentAvailable.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),60000, pendingIntent);



    }


    public void CancelAlarm(Context context)

    {

        AlarmManager alarmManager=(AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, GetIfSpeechContentAvailable.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.cancel(pendingIntent);

    }


    public void setOnetimeTimer(Context context) {

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, GetIfSpeechContentAvailable.class);

        intent.putExtra(ONE_TIME, Boolean.TRUE);

        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);

        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pi);

    }


}
