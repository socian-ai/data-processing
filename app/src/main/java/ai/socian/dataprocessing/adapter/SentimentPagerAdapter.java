package ai.socian.dataprocessing.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import ai.socian.dataprocessing.activity.sentiment.SentimentShowFragment;
import ai.socian.dataprocessing.model.SentimentContent;

/**
 * Created by tamzid on 2/5/18.
 */

public class SentimentPagerAdapter extends FragmentPagerAdapter {

    private static final String TAG = "MyFragmentPagerAdapter";
    private List<SentimentContent> mListsItem;
    private FragmentManager mFragmentManager;


    public SentimentPagerAdapter(FragmentManager fm, List<SentimentContent> listsItem) {
        super(fm);
        this.mFragmentManager = fm;
        this.mListsItem = listsItem;
    }

    @Override
    public int getCount() {
        return mListsItem.size();
    }

    @Override
    public Fragment getItem(int position) {

        List<Fragment> fragmentsList = mFragmentManager.getFragments();

//        SpeechShowFragment fragment1 = new SpeechShowFragment();
        SentimentShowFragment fragment1 = new SentimentShowFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        fragment1.setArguments(bundle);

        return fragment1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(1+position);
    }


    @Override
    public int getItemPosition(Object object) {
        int position = mListsItem.size();

        if (position >= 0) {
            return POSITION_UNCHANGED;
        } else {
            return POSITION_NONE;
        }
    }


}