package ai.socian.dataprocessing.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import ai.socian.dataprocessing.activity.speech.SpeechShowFragment;
import ai.socian.dataprocessing.model.SpeechContent;

/**
 * Created by tamzid on 1/24/18.
 */

public class SpeechPagerAdapter extends FragmentPagerAdapter {

    private static final String TAG = "MyFragmentPagerAdapter";
    private List<SpeechContent> mListsItem;
    private FragmentManager mFragmentManager;


    public SpeechPagerAdapter(FragmentManager fm, List<SpeechContent> listsItem) {
        super(fm);
        this.mFragmentManager = fm;
        this.mListsItem = listsItem;
    }

    @Override
    public int getCount() {
        return mListsItem.size();
    }

    @Override
    public Fragment getItem(int position) {

        List<Fragment> fragmentsList = mFragmentManager.getFragments();

        SpeechShowFragment fragment1 = new SpeechShowFragment();
//        SentimentShowFragment fragment1 = new SentimentShowFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        fragment1.setArguments(bundle);

        return fragment1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(1+position);
    }


    @Override
    public int getItemPosition(Object object) {
        int position = mListsItem.size();

        if (position >= 0) {
            return POSITION_UNCHANGED;
        } else {
            return POSITION_NONE;
        }
    }


}