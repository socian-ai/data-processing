package ai.socian.dataprocessing.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.model.SentimentContent;

/**
 * Created by tamzid on 2/6/18.
 */

public class SentimentListAdapter extends RecyclerView.Adapter<SentimentListAdapter.MyViewHolder> {

    private List<SentimentContent> speechContentsList;
    Context context;
    int targetViewPosition ;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView contentID, contentText;
        public ImageView goButton;
        public CardView cardview;
        public View lineView;

        public MyViewHolder(View view) {
            super(view);
            contentID = (TextView) view.findViewById(R.id.content_id);
            contentText = (TextView) view.findViewById(R.id.content_text);
            cardview = (CardView) view.findViewById(R.id.cardview);
            lineView = (View) view.findViewById(R.id.line_view);
//            goButton =  (ImageView) view.findViewById(R.id.button);
//            year = (TextView) view.findViewById(R.id.year);
        }
    }


    public SentimentListAdapter(Context context ,List<SentimentContent> speechContentsList,int position) {
        this.speechContentsList = speechContentsList;
        this.context = context;
        this.targetViewPosition = position;
    }

    @Override
    public SentimentListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expectanim_cell, parent, false);

        return new SentimentListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SentimentListAdapter.MyViewHolder holder, final int position) {
        SentimentContent model = speechContentsList.get(position);
        holder.contentID.setText(model.getId()+"");
        holder.contentText.setText(model.getContent()+"");

        if (targetViewPosition == position ){
            holder.cardview.setCardBackgroundColor(context.getResources().getColor(R.color.primary));
            holder.contentID.setTextColor(context.getResources().getColor(R.color.White));
            holder.contentText.setTextColor(context.getResources().getColor(R.color.White));
            holder.lineView.setBackgroundColor(context.getResources().getColor(R.color.White));
        }else {
            holder.cardview.setCardBackgroundColor(context.getResources().getColor(R.color.White));
            holder.contentID.setTextColor(context.getResources().getColor(R.color.primary_text));
            holder.contentText.setTextColor(context.getResources().getColor(R.color.primary_text));
            holder.lineView.setBackgroundColor(context.getResources().getColor(R.color.black));
        }
//        holder.goButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent =  new Intent(context, SpeechTagActivity.class);
//
//                intent.putExtra(Constant.SPEECH_POSITION,position);
//                context.startActivity(intent);
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return speechContentsList.size();
    }
}