package ai.socian.dataprocessing.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.model.User;
import ai.socian.dataprocessing.util.ApplicationSingleton;
import ai.socian.dataprocessing.util.Constant;

public class ProfileActivity extends AppCompatActivity {
    User userModel;
    DB snappydb;

    TextView name,email,nid,phone_number,bkash_number,  rocket_number,date_of_birth,gender;
    String nameText,emailText,nidText,phone_numberText,bkash_numberText,  rocket_numberText,date_of_birthText,genderText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile");
//        TextView toolbarText = (TextView) findViewById(R.id.toolbar_text);
//        if (toolbarText != null && toolbar != null) {
//            toolbarText.setText("Profile");
//            setSupportActionBar(toolbar);
//        }



        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

             nameText = model.getName();
             emailText = model.getEmail();
             nidText = model.getNID();
             phone_numberText = model.getMobileNumber();
             bkash_numberText = model.getBkashNumber();
             rocket_numberText = model.getRocketNumber();
             date_of_birthText = model.getBirthday();
             genderText = model.getGender();

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        name = (TextView) findViewById(R.id.name);
        email = (TextView) findViewById(R.id.email);
        nid = (TextView) findViewById(R.id.nid);
        phone_number = (TextView) findViewById(R.id.phone_number);
        bkash_number = (TextView) findViewById(R.id.bkash_number);
        rocket_number = (TextView) findViewById(R.id.rocket_number);
        date_of_birth = (TextView) findViewById(R.id.date_of_birth);
        gender = (TextView) findViewById(R.id.gender);


        name.setText(htmlConvert("Name",nameText));
        email.setText(htmlConvert("Email",emailText));
        nid.setText(htmlConvert("NID",nidText));
        phone_number.setText(htmlConvert("Phone Number",phone_numberText));
        bkash_number.setText(htmlConvert("Bkash Number",bkash_numberText));
        rocket_number.setText(htmlConvert("Rocket Number",rocket_numberText));
        date_of_birth.setText(htmlConvert("Date of Birth",date_of_birthText));
        gender.setText(htmlConvert("Gender",genderText));





    }


    public Spanned htmlConvert(String fieldName, String fieldValue) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Spanned htmlAsSpanned = Html.fromHtml("<font color='#303030'><b>" + fieldName + "&nbsp;&nbsp;:</b></font>&nbsp;&nbsp;" + fieldValue, Html.FROM_HTML_MODE_COMPACT);
            return htmlAsSpanned;
        } else {
            Spanned htmlAsSpanned = Html.fromHtml("<font color='#303030'><b>" + fieldName + "&nbsp;&nbsp;:</b></font>&nbsp;&nbsp;" + fieldValue);

            return htmlAsSpanned;

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile_edit_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:

                Intent intent = new Intent(ProfileActivity.this,ProfileEditActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);


                break;
            default:
                return false;
        }
        return true;
    }
}
