package ai.socian.dataprocessing.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.util.CommonActivity;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class TermsAndConditionsActivity extends CommonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_terms_and_conditions);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Rules & Conditions");

        TextView terms_tv = (TextView) findViewById(R.id.terms_tv);

        String htmlText = "<p>\n" +
                "    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"><br style=\"color: rgb(0, 0, 0); font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;\"><span style='color: rgb(0, 0, 0); font-family: \"Helvetica Neue\", \"Segoe UI\", Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;'>সুপ্রিয় বাংলাভাষীগণ। বাংলা ভাষাকে আমরা সবাই ভালোবাসি। সেই ভাষাকে ভালোবেসে সেন্টিমেন্ট আর ইমোশন ডাটা ল্যাবেলিং এর জন্য ক্রাউডসোর্সিং ১ টি প্লাটফর্ম এই ডাটা প্রোসেসিং অ্যাপটি। আর যারাই এই অ্যাপে ডাটা দিয়ে অবদান রাখবেন এবং বাংলা ভাষা সমৃদ্ধকরণে কাজ করবেন, তাদের সবার নাম লেখা থাকবে বাংলা ভাষার উন্নয়নকল্পে কন্ট্রিবিউটরদের তালিকায়। আর আপনাদের সহায়তায়ই দিন শেষে তৈরি হবে অসাধারণ এক কৃত্তিম বুদ্ধিমত্তা।&nbsp;</span><br style=\"color: rgb(0, 0, 0); font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;\"><br style=\"color: rgb(0, 0, 0); font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;\"><span style='color: rgb(0, 0, 0); font-family: \"Helvetica Neue\", \"Segoe UI\", Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;'>আর সেন্টিমেন্ট এবং ইমোশন ল্যাবেলিং এর জন্য সোশিয়ানের ১টি পেইড ক্যাম্পেইন খুব শীঘ্রই আসছে। তাই এই অ্যাপে নিয়ম বুঝে এখন যারা ভালো কাজ করবেন, তাদেরকে পেইড ক্যাম্পেইনেও এড করা হবে। পেইড ক্যাম্পেইনে এলিজিবল হওয়ার জন্য এই অ্যাপে সঠিকভাবে কমপক্ষে ১০০ টি সেন্টিমেন্ট ডাটা এবং ৫০ টি ইমোশন ডাটা দিতে হবে। কেউ বেশি চাইলে বেশিও করতে পারবেন। এখানে যারা ভালো পারফরমেন্স করবেন, তারা পরবর্তি পেইড ক্যাম্পেইনে কাজ করার জন্য অগ্রাধিকার পাবেন। আর কন্ট্রিবিউটরদের তালিকায় আপনাদের নামতো থাকছেই।</span><br style=\"color: rgb(0, 0, 0); font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;\"><br style=\"color: rgb(0, 0, 0); font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;\"><span style='color: rgb(0, 0, 0); font-family: \"Helvetica Neue\", \"Segoe UI\", Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;'>যেই ভাষায় মনের ভাব প্রতিনিয়ত প্রকাশ করি, মায়ের থেকে শেখা যেই বুলি, সেই বাংলার জন্য এই সামান্য কাজটুকু কি আমরা করতে পারব না?</span></p>\n" +
                "<p><br></p>\n" +
                "<p>\n" +
                "    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"><span style='color: rgb(250, 197, 28); font-family: \"Helvetica Neue\", \"Segoe UI\", Helvetica, Arial, sans-serif; font-size: 20px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;'>সেন্টিমেন্ট রুলস - </span><br style=\"color: rgb(0, 0, 0); font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;\"><strong><br></strong></p>\n" +
                "<p><strong>\uFEFFপ্রতিটি বাক্যের জন্য মোট ৬ ধরনের সেন্টিমেন্ট অপশন থাকবে -:</strong></p>\n" +
                "<p><strong>1. Objective (No sentiment)</strong></p>\n" +
                "<p><strong>2. Neutral/Mixed sentiment</strong></p>\n" +
                "<p><strong>3. Strongly Positive sentiment</strong></p>\n" +
                "<p><strong>4. Weakly Positive sentiment</strong></p>\n" +
                "<p><strong>5. Strongly Negative sentiment</strong></p>\n" +
                "<p><strong>6. Weakly Negative sentiment</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>নিয়মাবলিঃ</strong></p>\n" +
                "<p><strong>বাক্যটি পড়ে নিচের নিয়ম অনুযায়ী উপরের ৬টি অপশনের মধ্যে যেই অপশনটি সবচেয়ে সঠিক মনে হয়, সেই অপশনটি সেলেক্ট করুন। সেন্টিমেন্টের ক্ষেত্রে আপনি শুধু ১ টি অপশনই সেলেক্ট করতে পারবেন।</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>১ । Objective (No sentiment) :</strong></p>\n" +
                "<p><strong>কোন বাক্যে যখন যেকোনো বিষয়ে বক্তব্য দেয়া হয় (facts , বর্ণনা , চিরন্তন সত্য ইত্যাদি) যেখানে speaker এর কোন অভিমত প্রকাশ পায় না, বরং কোন কিছু বর্ণনা করা হয় মাত্র, তখন সেটা এই category তে যাবে। যেমন : করোনা পরিস্থিতি মোকাবেলায় ন্যাশনাল গার্ড মোতায়েন করেছে নিউইয়র্কের গভর্নর। তিনি আইইডিসিআরে আয়োজিত নিয়মিত প্রেস ব্রিফিংয়ে এসব কথা বলেন। বাংলাদেশ দক্ষিণ এশিয়ার ক্ষুদ্র একটি দেশ।</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>২ । Neutral/Mixed sentiment :&nbsp;</strong></p>\n" +
                "<p><strong>i) কোন বাক্তি/ জিনিশের এমন কোন বৈশিষ্ট্য যদি বর্ণনা করা হয়, &nbsp;যেটা Positive/ Negative কোনটাই না তাহলেও এই catagory তে যাবে। যেমন: কলমটা ছোট। বাড়িটা অনেক বড়। &nbsp;লোকটাকে চেনা চেনা মনে হচ্ছে । ঢাকায় অনেক মসজিদ আছে। নিলয় ক্লাস টু তে পড়ে। &nbsp;</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>ii) প্রশ্নবোধক বাক্যে যদি কোন কিছু জানতে চাওয়া হয়, কোন ধরনের Positive/ Negative emotion প্রকাশ না পায় সেগুলো এই catagory তে যাবে। &nbsp; যেমন : আপনার নাম &nbsp;কি?, কে কে বাড়ির কাজ কর নি হাত তোল । ভাই, স্কুলটা কোন দিকে? ২ প্যাকেট কিনলে কি ১টা ফ্রি দিবেন?</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>iii) কোনও suggestion / request যেখানে Positive/ Negative emotion প্রকাশ না পায় সেগুলো এই catagory তে যাবে। যেমন : বেপারটা একটু দেখবেন। &nbsp;তুমি এটা ট্রাই করে দেখতে পার। একটা নতুন গান রিলিজ দেন না ভাই।</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>iv)কোনও বাক্যের ক্ষেত্রে বাক্যটি &nbsp;conflicting বা পরস্পরবিরোধী sentiment আছে বলে মনে হলে সেটা এই catagory তে যাবে। &nbsp;যেমন: &nbsp;&quot;কাজটা ভালই করেছ , তবে আরও একটু চেষ্টা করলে আরও ভাল হত &quot; এটা এই catagory তে যাবে। তবে খুব clearly বোঝা গেলে যেমন : &quot;ছেলেটার এমনিতে সবই ভাল , তবে চরিত্রে সমস্যা আছে। &quot; এটা negative হবে।</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>৩। Negative sentiment :</strong></p>\n" +
                "<p><strong>i) রাগ, ঘৃণা, বিরক্তি , &nbsp;হিংসাত্মক ও ধ্বংসমূলক কথাবার্তা প্রকাশ পেলে এই catagory তে যাবে। যেমন : সরকার কি এসব চোখে দেখে না ? , ওদের বাড়িঘর পুড়িয়ে দেশ থেকে বের করে দেওয়া উচিত। (SN) আল্লাহ তুমি কাফিরদের ধ্বংস করে দাও, আমিন (SN) &nbsp;আমি কি আপনার কাছে এটা জানতে চেয়েছি? (WN). &nbsp;এত টান ভালবাসা আসে কোথা থেকে ? (WN) &nbsp;সাহসী শব্দের অর্থ কি নগ্নতা!!? (SN)&nbsp;</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>ii) কোন বাক্তি/ জিনিশের এমন কোন বৈশিষ্ট্য যদি বর্ণনা করা হয় যেটা বক্তার পছন্দ না । যেমন : এত বাজে খেলা আমি জীবনেও দেখি নাই।(SN) বার্মিজদের চরিত্রই খারাপ।(SN)</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>৪। Positive sentiment :</strong></p>\n" +
                "<p><strong>i) প্রশংসা, আনন্দ, উদযাপন, সন্তোষ প্রকাশমূলক কথাবার্তা প্রকাশ পেলে এই catagory তে যাবে। যেমন : সাবাস বাংলাদেশ, এগিয়ে যাও তোমরা। তোমরা খুব &nbsp;ভাল ফলাফল করেছ (SP)। আল্লাহ তুমি সবাইকে হেফাজত করো আমিন (SP)</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>ii) double negative বাক্য হলেও সেটা positive হতে পারে । যেমন : বাংলাদেশ কিন্তু খারাপ খেলে নাই (WP), আমি জীবনেও তার নিন্দা করব না (SP)।</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>৫। Weakly vs Strongly:</strong></p>\n" +
                "<p><strong>i) যখন সুস্পষ্টভাবে &nbsp;speaker কারো প্রশংসা/নিন্দা করবে তখন সেটা Strongly Positive/Negative হবে। যেমন: বাংলালিংকের তো নেটওয়ার্কই থাকে না (Strongly Negative)।</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>ii)example (All sentiments in one sentence ):</strong></p>\n" +
                "<p><strong>Strongly Positive: ওরা বেশ ভাল কাজ করেছে। ওরা ভাল কাজ করেছে।</strong></p>\n" +
                "<p><strong>Weakly Positive: ওরা মোটামুটি ভাল কাজ করেছে।</strong></p>\n" +
                "<p><strong>Neutral: ওরা মোটামুটি কাজ করেছে।</strong></p>\n" +
                "<p><strong>Weakly Negative: ওরা খুব একটা ভাল কাজ করে নাই। ওরা খারাপ কাজ করেছে।</strong></p>\n" +
                "<p><strong>Strongly Negative: ওরা মোটেও ভাল কাজ করে নাই। ওরা খুবই খারাপ কাজ করেছে।</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>৬। Perspective: sentiment মূলত depend করবে speaker এর দৃষ্টিভঙ্গির উপর, কার সম্পর্কে কথা বলা হচ্ছে তার উপরে না। যেমনঃ ধুর ! গ্রামীনফোনের কল রেট এত বেশি। (SN) &nbsp;এখানে কল রেট বেশী হওয়ায় স্পিকার খেপে আছে মনে হচ্ছে দেখে এটা negative. &nbsp;বাংলালিংকের নেটওয়ার্ক খুবই বাজে। (SN)</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>৭। Sarcasm: সারকাসম / ব্যাঙ্গাত্বক sentence এর ক্ষেত্রে খুব বেশি গভীর অর্থ চিন্তা না করে আপাতত যে meaning বোঝা যাচ্ছে সেটার ভিত্তিতেই sentiment নিরধারিত হবে। ওহ , তুমি তো খুব ভাল মানুষ।</strong></p>\n" +
                "<p><strong>এখানে গভীর চিন্তা করলে এটাকে sarcastic / negative মনে হতে পারে। কিন্তু simply এটাকে আমরা positive sentence হিসেবেই চিহ্নিত করব।</strong></p>\n" +
                "<p><br></p>\n" +
                "<p>\n" +
                "    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"><span style='color: rgb(250, 197, 28); font-family: \"Helvetica Neue\", \"Segoe UI\", Helvetica, Arial, sans-serif; font-size: 20px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: pre-wrap; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(241, 240, 240); text-decoration-style: initial; text-decoration-color: initial;'>ইমোশন রুলস - </span></p>\n" +
                "<p><br></p>\n" +
                "<p><strong>\uFEFFসেন্টেন্স বা বাক্যের ইমোশন বুঝতে হলে ঐ বাক্য পড়ে আপনার কি মনে হচ্ছে বা বাক্যে কি বলা হচ্ছে, সেটা বুঝতে হবে। যেই ইমোশনগুলো বাক্যটি দিয়ে প্রকাশ পায়, সেই ইমোশনগুলো সেলেক্ট করুন।</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>মোট ৬ ধরনের ইমশন আছে -</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>১) Happy (খুশি)</strong></p>\n" +
                "<p><strong>২) Fear (ভীতি)</strong></p>\n" +
                "<p><strong>৩) Anger/Disgust (রাগ/ক্ষোভ)</strong></p>\n" +
                "<p><strong>৪) Sadness (কষ্ট)</strong></p>\n" +
                "<p><strong>৫) Surprise (বিস্ময়/অবাক)</strong></p>\n" +
                "<p><strong>৬) Neutral (কিছুই না)</strong></p>\n" +
                "<p><strong><br></strong></p>\n" +
                "<p><strong>একই বাক্যের মধ্যে এক বা একাধিক ইমোশন থাকতে পারে। তাই সেলেক্ট করার সময় ভালো ভাবে বাক্যটি পড়ে নিয়ে তারপরে ইমোশন ট্যাগ করুন।</strong></p>";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            terms_tv.setText(Html.fromHtml(htmlText, Html.FROM_HTML_MODE_COMPACT));
        }

        else {
            terms_tv.setText(Html.fromHtml(htmlText));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
