package ai.socian.dataprocessing.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.model.User;
import ai.socian.dataprocessing.util.ApplicationSingleton;
import ai.socian.dataprocessing.util.CommonActivity;
import ai.socian.dataprocessing.util.Constant;

public class ProfileEditActivity extends CommonActivity {


    @BindView(R.id.full_name)
    TextInputEditText full_name;

    @BindView(R.id.national_id)
    TextInputEditText national_id;

    @BindView(R.id.phone_number)
    TextInputEditText phone_number;

    @BindView(R.id.bkash_number)
    TextInputEditText bkash_number;

    @BindView(R.id.rocket_number)
    TextInputEditText rocket_number;

    @BindView(R.id.email)
    TextInputEditText email;

    @BindView(R.id.password)
    TextInputEditText password;

    @BindView(R.id.confirm_password)
    TextInputEditText confirm_password;


    @BindView(R.id.date_of_birth)
    TextInputEditText date_of_birth;

    @BindView(R.id.radio_group)
    RadioGroup radio_group;

    String gender;

    @BindView(R.id.radio_male)
    RadioButton radioMale;


    @BindView(R.id.radio_female)
    RadioButton radioFemale;


    @BindView(R.id.cancel_btn)
    AppCompatButton cancel_btn;

    @BindView(R.id.save_btn)
    AppCompatButton save_btn;


    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    String selectedDate = "";

    SweetAlertDialog pogressDialog;
    User userModel;
    DB snappydb;
    String nameText, emailText, nidText, phone_numberText, bkash_numberText, rocket_numberText, date_of_birthText, genderText;
    String token;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        ButterKnife.bind(this);

        try {
            snappydb = DBFactory.open(getApplicationContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);
            token = model.getToken();
            id = model.getUserID();
            System.out.print("USER_ID :" +id);
            System.out.print("USER_TOKEN :" +token);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }



        date_of_birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDateInsanse();
                new DatePickerDialog(ProfileEditActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        gender = "Male";
        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == radioMale.getId()) {
                    gender = "Male";
                } else if (i == radioFemale.getId()) {
                    gender = "Female";
                }
            }
        });

//        date_of_birth.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if (b) {
//                    getDateInsanse();
//                    new DatePickerDialog(ProfileEditActivity.this, date, myCalendar
//                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
////                    updateLabel();
//                }
//            }
//        });


        radioMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileEditActivity.this, ProfileActivity.class);
                startActivity(intent);
                finish();
            }
        });


        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                save();

            }
        });


        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            nameText = model.getName();
            emailText = model.getEmail();
            nidText = model.getNID();
            phone_numberText = model.getMobileNumber();
            bkash_numberText = model.getBkashNumber();
            rocket_numberText = model.getRocketNumber();
            date_of_birthText = model.getBirthday();
            genderText = model.getGender();

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }


        full_name.setText(nameText + "");
        email.setText(emailText + "");
        national_id.setText(nidText + "");
        phone_number.setText(phone_numberText + "");
        bkash_number.setText(bkash_numberText + "");
        rocket_number.setText(rocket_numberText + "");
        date_of_birth.setText(date_of_birthText + "");

        if (genderText.equals("Male")) {
            radioFemale.setChecked(false);
            radioMale.setChecked(true);

        } else if (genderText.equals("Female")) {
            radioMale.setChecked(false);
            radioFemale.setChecked(true);
        }


    }

    public void save() {

        pogressDialog = new SweetAlertDialog(ProfileEditActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pogressDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
        pogressDialog.setTitleText("Loading");
        pogressDialog.setCancelable(false);
        pogressDialog.show();


        if (!isOnline()) {
            pogressDialog.dismiss();

            new SweetAlertDialog(ProfileEditActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Opss!!!")
                    .setContentText("Please Active Your Internet Connection")
                    .setConfirmText("Okay!")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();
            return;
        }

        if (!password.getText().toString().equals(confirm_password.getText().toString())) {
            pogressDialog.dismiss();
            confirm_password.setError("The password doesn't match");
            return;

        }



        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        if (!full_name.getText().toString().isEmpty()) {
            params.put("name", full_name.getText().toString());
        }
        if (!email.getText().toString().isEmpty()) {
            params.put("email", email.getText().toString());

        }
        if (!password.getText().toString().isEmpty()) {
            params.put("password", password.getText().toString());
        }
        if (!national_id.getText().toString().isEmpty()) {
            params.put("NID", national_id.getText().toString());
        }
        if (!bkash_number.getText().toString().isEmpty()) {
            params.put("bikash_number", bkash_number.getText().toString());
        }
        if (!rocket_number.getText().toString().isEmpty()) {
            params.put("rocket_number", rocket_number.getText().toString());
        }
        if (!gender.isEmpty()) {
            params.put("gender", gender);
        }
        if (!date_of_birth.getText().toString().isEmpty()) {
            params.put("birth_day", date_of_birth.getText().toString());
        }


        client.post("http://socian-data-processing.southeastasia.cloudapp.azure.com/socian_data_processing/socian_data_processing/public/profile_update/"+id+"?api_token="+token, params, new JsonHttpResponseHandler() {


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                try {
                    if (response.getString("code").equals("200")) {


                        try {
                            JSONObject user = new JSONObject();
                            user = response.getJSONObject("user");
                            int id = user.getInt("id");
                            String name = user.getString("name");
                            String email = user.getString("email");
                            String api_token = user.getString("api_token");
                            String NID = user.getString("NID");
                            String mobile_number = user.getString("mobile_number");
                            String bikash_number = user.getString("bikash_number");
                            String rocket_number = user.getString("rocket_number");
                            String gender = user.getString("gender");
                            String birth_day = user.getString("birth_day");
                            String created_at = user.getString("created_at");
                            String updated_at = user.getString("updated_at");

                            userModel = new User();
                            userModel.setUserID(id);
                            userModel.setName(name);
                            userModel.setEmail(email);
                            userModel.setToken(api_token);
                            userModel.setNID(NID);
                            userModel.setMobileNumber(mobile_number);
                            userModel.setBkashNumber(bikash_number);
                            userModel.setRocketNumber(rocket_number);
                            userModel.setGender(gender);
                            userModel.setBirthday(birth_day);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {
                            snappydb = DBFactory.open(ApplicationSingleton.getContext());
                            snappydb.put(Constant.USER_MODEL, userModel);


                            User model = new User();
                            model = snappydb.get(Constant.USER_MODEL, User.class);

                            String token = model.getToken();


                            Log.i(TAG, "TOKEN:" + token);

                            snappydb.close();

                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }

                        pogressDialog.dismiss();
                        new SweetAlertDialog(ProfileEditActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Saved!")
                                .setContentText("Your Profile Updated.")
                                .setConfirmText("GO")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Intent intent = new Intent(ProfileEditActivity.this, ProfileActivity.class);
                                        sweetAlertDialog.dismiss();
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                                .show();
                    } else if (response.getString("code").equals("405")){

                        pogressDialog.dismiss();
                        new SweetAlertDialog(ProfileEditActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Token Mismatched")
                                .setConfirmText("Log in again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        logout();
                                    }
                                })
                                .show();



                    }else {

                        pogressDialog.dismiss();
                        new SweetAlertDialog(ProfileEditActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Sorry, something went wrong.")
                                .setConfirmText("try again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                pogressDialog.dismiss();
                new SweetAlertDialog(ProfileEditActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Opss!!!")
                        .setContentText("Sorry, something went wrong.")
                        .setConfirmText("try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                pogressDialog.dismiss();
                new SweetAlertDialog(ProfileEditActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Opss!!!")
                        .setContentText("Sorry, something went wrong.")
                        .setConfirmText("try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();

                }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                pogressDialog.dismiss();
                new SweetAlertDialog(ProfileEditActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Opss!!!")
                        .setContentText("Sorry, something went wrong.")
                        .setConfirmText("try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();

            }
        });
    }


    private void getDateInsanse() {
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy";


                selectedDate = new SimpleDateFormat(myFormat, Locale.US).format(myCalendar.getTime());
                date_of_birth.setText(selectedDate);

            }

        };
    }

}
