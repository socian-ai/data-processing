package ai.socian.dataprocessing.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.adapter.SpeechListAdapter;
import ai.socian.dataprocessing.adapter.SpeechPagerAdapter;
import ai.socian.dataprocessing.helper.RecyclerTouchListener;
import ai.socian.dataprocessing.model.SpeechContent;
import ai.socian.dataprocessing.util.ApplicationSingleton;
import ai.socian.dataprocessing.util.CommonActivity;

public class SpeechActivity extends CommonActivity {

    ViewPager viewPager;
    public SpeechPagerAdapter myAdapter;
    List<SpeechContent> list = new ArrayList<>();
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};
    private boolean permissionToRecordAccepted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speech_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Tag Speech Data");

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        list = new ArrayList<>();
        list = getSpeechListStorageData();


        if (list == null || list.size() < 0) {
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.setTitleText("There is no content!")
                    .setConfirmText("Try again later")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    })

                    .show();
        } else {

            myAdapter = new SpeechPagerAdapter(getSupportFragmentManager(), list);
            viewPager.setAdapter(myAdapter);
        }

//        viewPager.setCurrentItem(4);


        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }

    public void updateFragment(int position) {


        Intent intent = new Intent(ApplicationSingleton.getContext(), SpeechActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted) finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_list:
                listDialogShow();
                break;
            default:
                return false;
        }
        return true;
    }


    public RecyclerView recycler_view;
    public SpeechListAdapter adapter;
    public TextView no_content_tv;

    public void listDialogShow() {
        final Dialog dialog = new Dialog(SpeechActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_speech_list);
        dialog.setTitle("Add Info");

        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setGravity(Gravity.RIGHT);
        recycler_view = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        no_content_tv = (TextView) dialog.findViewById(R.id.no_content_tv);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setHasFixedSize(true);
        recycler_view.setNestedScrollingEnabled(false);
        recycler_view.setItemAnimator(new DefaultItemAnimator());

        if (!(list == null)) {
            int position = viewPager.getCurrentItem();
            recycler_view.getLayoutManager().scrollToPosition(position);
            adapter = new SpeechListAdapter(SpeechActivity.this, list, position);
            recycler_view.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            no_content_tv.setVisibility(View.VISIBLE);
        }

        dialog.show();

        recycler_view.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recycler_view, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                dialog.dismiss();
                viewPager.setCurrentItem(position);
                myAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }
}
