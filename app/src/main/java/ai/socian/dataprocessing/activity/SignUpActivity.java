package ai.socian.dataprocessing.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.model.User;
import ai.socian.dataprocessing.util.ApplicationSingleton;
import ai.socian.dataprocessing.util.CommonActivity;
import ai.socian.dataprocessing.util.Constant;

public class SignUpActivity extends CommonActivity {


    @BindView(R.id.full_name)
    TextInputEditText full_name;

    @BindView(R.id.national_id)
    TextInputEditText national_id;

    @BindView(R.id.phone_number)
    TextInputEditText phone_number;

    @BindView(R.id.bkash_number)
    TextInputEditText bkash_number;

    @BindView(R.id.rocket_number)
    TextInputEditText rocket_number;

    @BindView(R.id.email)
    TextInputEditText email;

    @BindView(R.id.password)
    TextInputEditText password;

    @BindView(R.id.confirm_password)
    TextInputEditText confirm_password;


    @BindView(R.id.date_of_birth)
    TextInputEditText date_of_birth;

    @BindView(R.id.radio_group)
    RadioGroup radio_group;

    String gender;

    @BindView(R.id.radio_male)
    RadioButton radioMale;


    @BindView(R.id.radio_female)
    RadioButton radioFemale;


    @BindView(R.id.signup_btn)
    AppCompatButton signup_btn;

    @BindView(R.id.back_button)
    AppCompatButton back_button;


    AppCompatCheckBox terms_cb;
    TextView terms_tv;


    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    String selectedDate = "";

    SweetAlertDialog pogressDialog;
    User userModel;
    DB snappydb;
    String nameText, emailText, nidText, phone_numberText, bkash_numberText, rocket_numberText, date_of_birthText, genderText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);


        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            nameText = model.getName();
            emailText = model.getEmail();
            nidText = model.getNID();
            phone_numberText = model.getMobileNumber();
            bkash_numberText = model.getBkashNumber();
            rocket_numberText = model.getRocketNumber();
            date_of_birthText = model.getBirthday();
            genderText = model.getGender();

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        date_of_birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDateInsanse();
                new DatePickerDialog(SignUpActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        gender = "Male";
        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == radioMale.getId()) {
                    gender = "Male";
                } else if (i == radioFemale.getId()) {
                    gender = "Female";
                }
            }
        });

        date_of_birth.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    getDateInsanse();
                    new DatePickerDialog(SignUpActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
//                    updateLabel();
                }
            }
        });


        radioMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });



        terms_cb = (AppCompatCheckBox) findViewById(R.id.terms_cb);
        terms_tv = (TextView) findViewById(R.id.terms_tv);

        terms_tv.setText("I agree to the Rules & Conditions");
        setUnderLineText(terms_tv, "Rules & Conditions");

        terms_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUpActivity.this, TermsAndConditionsActivity.class));
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });



        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!terms_cb.isChecked()) {
                    new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Opss!!!")
                            .setContentText("You  need to agree to the Terms and Conditions to continue!")
                            .setConfirmText("Okay!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }

                else {
                    pogressDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    pogressDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
                    pogressDialog.setTitleText("Loading");
                    pogressDialog.setCancelable(false);
                    pogressDialog.show();


                    if (!isOnline()) {
                        pogressDialog.dismiss();

                        new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Please Active Your Internet Connection")
                                .setConfirmText("Okay!")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();
                        return;
                    }

                    if (!password.getText().toString().equals(confirm_password.getText().toString())) {
                        pogressDialog.dismiss();
                        confirm_password.setError("The password doesn't match");
                        return;

                    }

                    AsyncHttpClient client = new AsyncHttpClient();

                    RequestParams params = new RequestParams();
                    params.put("name", full_name.getText().toString());
                    params.put("email", email.getText().toString());
                    params.put("password", password.getText().toString());
                    params.put("NID", national_id.getText().toString());
                    params.put("mobile_number", phone_number.getText().toString());
                    params.put("bikash_number", bkash_number.getText().toString());
                    params.put("rocket_number", rocket_number.getText().toString());
                    params.put("gender", gender);
                    params.put("birth_day", date_of_birth.getText().toString());

                    client.post(Constant.register, params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                            Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                            try {
                                if (response.getString("code").equals("200")) {

                                    pogressDialog.dismiss();
                                    new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText("Account Created Successfully!!!")
                                            .setContentText("Login Now!")
                                            .setConfirmText("OK")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                                                    intent.putExtra("number", phone_number.getText().toString());
                                                    sweetAlertDialog.dismiss();
                                                    startActivity(intent);
                                                    finish();

                                                }
                                            })
                                            .show();
                                } else {

                                    pogressDialog.dismiss();
                                    new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Opss!!!")
                                            .setContentText("Sorry, something went wrong.")
                                            .setConfirmText("try again")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sweetAlertDialog.dismiss();
                                                }
                                            })
                                            .show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                            Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + errorResponse.toString());

                            pogressDialog.dismiss();
                            new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Opss!!!")
                                    .setContentText("Sorry, something went wrong.")
                                    .setConfirmText("try again")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            super.onFailure(statusCode, headers, responseString, throwable);
                            Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + responseString);

                            pogressDialog.dismiss();
                            new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Opss!!!")
                                    .setContentText("Sorry, something went wrong.")
                                    .setConfirmText("try again")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();
                        }

                    });

                }

            }
        });

    }

    private void getDateInsanse() {
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy";

                selectedDate = new SimpleDateFormat(myFormat, Locale.US).format(myCalendar.getTime());
                date_of_birth.setText(selectedDate);

            }

        };
    }


    public void setUnderLineText(TextView tv, String textToUnderLine) {
        String tvt = tv.getText().toString();
        int ofe = tvt.indexOf(textToUnderLine, 0);

        UnderlineSpan underlineSpan = new UnderlineSpan();
        SpannableString wordToSpan = new SpannableString(tv.getText());
        for (int ofs = 0; ofs < tvt.length() && ofe != -1; ofs = ofe + 1) {
            ofe = tvt.indexOf(textToUnderLine, ofs);
            if (ofe == -1)
                break;
            else {
                wordToSpan.setSpan(underlineSpan, ofe, ofe + textToUnderLine.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv.setText(wordToSpan, TextView.BufferType.SPANNABLE);
            }
        }
    }


}
