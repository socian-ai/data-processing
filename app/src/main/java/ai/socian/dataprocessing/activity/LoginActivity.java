package ai.socian.dataprocessing.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import io.fabric.sdk.android.Fabric;
import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.model.User;
import ai.socian.dataprocessing.services.GetIfSpeechContentAvailable;
import ai.socian.dataprocessing.util.ApplicationSingleton;
import ai.socian.dataprocessing.util.CommonActivity;
import ai.socian.dataprocessing.util.Constant;

public class LoginActivity extends CommonActivity {

    private GetIfSpeechContentAvailable alarm;
    @BindView(R.id.phone_number)
    EditText phoneNumber;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.login_btn)
    AppCompatButton loginBtn;
    @BindView(R.id.signup_btn)
    AppCompatButton sigupBtn;
    @BindView(R.id.forgot_pass)
    TextView forgotPassView;

    public static final String TAG = "LOG_IN";
    SweetAlertDialog pogressDialiog;
    User userModel;
    DB snappydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            String token = model.getToken();

            if (!token.isEmpty()) {
                startActivity(new Intent(this, MainActivity.class));
                finish();


            }
            Log.i(TAG, "TOKEN:" + token);

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }


        sigupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);

            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                pogressDialiog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                pogressDialiog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
                pogressDialiog.setTitleText("Loading");
                pogressDialiog.setCancelable(false);
                pogressDialiog.show();


                if (!isOnline()) {
                    pogressDialiog.dismiss();

                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Opss!!!")
                            .setContentText("Please Active Your Internet Connection")
                            .setConfirmText("Okay!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }


                AsyncHttpClient client = new AsyncHttpClient();

                RequestParams params = new RequestParams();
                params.put("mobile_number", phoneNumber.getText().toString());
                params.put("password", password.getText().toString());
                client.post(Constant.login, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                        try {
                            if (response.getString("code").equals("200")) {


                                try {
                                    JSONObject user = new JSONObject();
                                    user = response.getJSONObject("user");
                                    int id = user.getInt("id");
                                    String name = user.getString("name");
                                    String email = user.getString("email");
                                    String api_token = user.getString("api_token");
                                    String NID = user.getString("NID");
                                    String mobile_number = user.getString("mobile_number");
                                    String bikash_number = user.getString("bikash_number");
                                    String rocket_number = user.getString("rocket_number");
                                    String gender = user.getString("gender");
                                    String birth_day = user.getString("birth_day");
                                    String created_at = user.getString("created_at");
                                    String updated_at = user.getString("updated_at");


                                    userModel = new User();
                                    userModel.setUserID(id);
                                    userModel.setName(name);
                                    userModel.setEmail(email);
                                    userModel.setToken(api_token);
                                    userModel.setNID(NID);
                                    userModel.setMobileNumber(mobile_number);
                                    userModel.setBkashNumber(bikash_number);
                                    userModel.setRocketNumber(rocket_number);
                                    userModel.setGender(gender);
                                    userModel.setBirthday(birth_day);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                try {

                                    snappydb.put(Constant.USER_MODEL, userModel);


                                    User model = new User();
                                    model = snappydb.get(Constant.USER_MODEL, User.class);

                                    String token = model.getToken();


                                    Log.i(TAG, "TOKEN:" + token);

                                    snappydb.close();

                                } catch (SnappydbException e) {
                                    e.printStackTrace();
                                }
                                loadDataIfnotAvailable();

                                points(Constant.point_speech);
                                points(Constant.point_sentiment);

                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Do something after 5s = 5000ms
                                        pogressDialiog.dismiss();
                                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Success!!!")
                                                .setContentText("You logged in successfully")
                                                .setConfirmText("GO")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                        sweetAlertDialog.dismiss();
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                })
                                                .show();
                                    }
                                }, 3000);


                            } else {

                                pogressDialiog.dismiss();
                                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Opss!!!")
                                        .setContentText("Sorry, something went wrong.")
                                        .setConfirmText("try again")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismiss();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        pogressDialiog.dismiss();
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Sorry, something went wrong.")
                                .setConfirmText("try again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();

                    }
                });

            }
        });


        forgotPassView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogForForgetPass();
            }
        });

    }

    public void dialogForForgetPass() {

        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.forgot_password_layout);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);

        final TextInputEditText mobile_number = (TextInputEditText) dialog.findViewById(R.id.mobile_number);
        final TextInputEditText new_password = (TextInputEditText) dialog.findViewById(R.id.new_password);
        final TextInputEditText confirm_password = (TextInputEditText) dialog.findViewById(R.id.confirm_password);

        Button send_btn = (Button) dialog.findViewById(R.id.send_btn);
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!new_password.getText().toString().equals(confirm_password.getText().toString())) {

                    confirm_password.setError("The password doesn't matched");
                    return;

                }

                final SweetAlertDialog pogressDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                pogressDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
                pogressDialog.setTitleText("Loading");
                pogressDialog.setCancelable(false);
                pogressDialog.show();
                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                params.put("mobile_number", mobile_number.getText().toString());
                params.put("new_password", new_password.getText().toString());


                client.post(Constant.forgot_password, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());



                            pogressDialog.dismiss();
                            new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Verify!!!")
                                    .setContentText("Verify your account")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            viewForgotPassPin(mobile_number.getText().toString() + "");
                                            dialog.dismiss();
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();


                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        pogressDialog.dismiss();
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Verify!!!")
                                .setContentText("Verify your account")
                                .setConfirmText("OK")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        viewForgotPassPin(mobile_number.getText().toString() + "");
                                        dialog.dismiss();
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        pogressDialog.dismiss();
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Verify!!!")
                                .setContentText("Verify your account")
                                .setConfirmText("OK")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        viewForgotPassPin(mobile_number.getText().toString() + "");
                                        dialog.dismiss();
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();
                    }
                });



            }
        });

        dialog.show();
    }

    public void viewForgotPassPin(final String MobileNumber) {

        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.forgot_password_confirm_layout);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);

        final TextInputEditText pin_number = (TextInputEditText) dialog.findViewById(R.id.pin_number);

        Button send_btn = (Button) dialog.findViewById(R.id.send_btn);
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pin_number.getText().toString().isEmpty()){


                    pin_number.setError("The password doesn't matched");

                    return;
                }

                confirmPassword(dialog,MobileNumber,pin_number);
            }
        });

        dialog.show();


    }

    public  void confirmPassword(final Dialog dialog, String mobileNumber,TextInputEditText pin_number){
        final SweetAlertDialog pogressDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pogressDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
        pogressDialog.setTitleText("Loading");
        pogressDialog.setCancelable(false);
        pogressDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("mobile_number",mobileNumber);
        params.put("security_key", pin_number.getText().toString());


        client.post(Constant.password_updated, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());



                pogressDialog.dismiss();
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Success!!!")
                        .setContentText("Successfully updated your password")
                        .setConfirmText("Log in")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                dialog.dismiss();
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                pogressDialog.dismiss();
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Opss!!!")
                        .setContentText("Sorry, something went wrong.")
                        .setConfirmText("try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();

                            }
                        })
                        .show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                pogressDialog.dismiss();
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Opss!!!")
                        .setContentText("Sorry, something went wrong.")
                        .setConfirmText("try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    protected void onResume() {

        super.onResume();
        alarm = new GetIfSpeechContentAvailable();
        cancelRepeatingTimer();
    }

    public void cancelRepeatingTimer() {
        Context context = ApplicationSingleton.getContext();

        if (alarm != null) {

            alarm.CancelAlarm(context);

        } else {

            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();

        }

    }
}
