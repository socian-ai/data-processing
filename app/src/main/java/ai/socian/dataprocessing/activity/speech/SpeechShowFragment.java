package ai.socian.dataprocessing.activity.speech;

import android.Manifest;
import android.content.Intent;
import android.graphics.Paint;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import info.kimjihyok.ripplelibrary.Rate;
import info.kimjihyok.ripplelibrary.VoiceRippleView;
import info.kimjihyok.ripplelibrary.listener.RecordingListener;
import info.kimjihyok.ripplelibrary.renderer.Renderer;
import info.kimjihyok.ripplelibrary.renderer.TimerCircleRippleRenderer;
import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.activity.LoginActivity;
import ai.socian.dataprocessing.activity.SpeechActivity;
import ai.socian.dataprocessing.model.SpeechContent;
import ai.socian.dataprocessing.model.User;
import ai.socian.dataprocessing.util.ApplicationSingleton;
import ai.socian.dataprocessing.util.CommonActivity;
import ai.socian.dataprocessing.util.Constant;
import ai.socian.dataprocessing.util.SharedPreferencesData;


public class SpeechShowFragment extends Fragment {

    private static final String TAG = "SpeechTagActivity";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static final String DIRECTORY_NAME = "AudioCache";
    SharedPreferencesData sharedPreferencesData;
    private String token;
    DB snappydb;
    private MediaPlayer player = null;
    private File directory = null;
    //    private File audioFile = null;
    SweetAlertDialog pogressDialiog;

    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};
    private Renderer currentRenderer;

    VoiceRippleView voiceRipple;


    ImageView playButton;
    Button submitBtn, skipBtn;
    int position = 0;

    CountDownTimer ctimer;
    TextView contentId;
    TextView contentText;
    TextView timer;
    TextView player_duration_tv;


    List<SpeechContent> speechContentList = new ArrayList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        position = getArguments().getInt("position");
    }


    public static SpeechShowFragment newInstance(int page) {
        SpeechShowFragment fragmentFirst = new SpeechShowFragment();

        SpeechShowFragment fragment1 = new SpeechShowFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", page);
        fragment1.setArguments(bundle);
        return fragment1;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View speechView = inflater.inflate(R.layout.fragment_speech_show, container, false);

        timer = (TextView)  speechView.findViewById(R.id.timer);

        ctimer = new CountDownTimer( Long.MAX_VALUE , 1000) {
            int cnt = 0;
            @Override
            public void onTick(long millisUntilFinished) {

                cnt++;
                String time = new Integer(cnt).toString();

                long millis = cnt;
                int seconds = (int) (millis / 60);
                int minutes = seconds / 60;
                seconds     = seconds % 60;

                timer.setText(String.format("%d:%02d:%02d", minutes, seconds,millis));

            }

            @Override
            public void onFinish() {  cnt=0;          }
        };

        contentId = (TextView) speechView.findViewById(R.id.content_id);
        contentText = (TextView) speechView.findViewById(R.id.content_text);


        CommonActivity commonActivity = new CommonActivity();

        speechContentList = commonActivity.getSpeechListStorageData();

        try {
            snappydb = DBFactory.open(getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);
            token = model.getToken();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }


        contentId.setText("ID : " + speechContentList.get(position).getId() + "");
        contentText.setText(speechContentList.get(position).getContent() + "");


        playButton = (ImageView) speechView.findViewById(R.id.play_button);
        playButton.setVisibility(View.GONE);
        timer.setVisibility(View.GONE);

        player_duration_tv = (TextView) speechView.findViewById(R.id.player_duration_tv);
        player_duration_tv.setVisibility(View.GONE);


        submitBtn = (Button) speechView.findViewById(R.id.submit_button);
        skipBtn = (Button) speechView.findViewById(R.id.skip_button);

        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearRow(position);
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pogressDialiog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
                pogressDialiog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
                pogressDialiog.setTitleText("Loading");
                pogressDialiog.setCancelable(false);
                pogressDialiog.show();

                SubmitData();
            }
        });


        submitBtn.setVisibility(View.GONE);
        skipBtn.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);


        player = new MediaPlayer();
        // directory = new File(Environment.getExternalStorageDirectory(), DIRECTORY_NAME);
        directory = new File(getContext().getExternalCacheDir().getAbsolutePath(), DIRECTORY_NAME);

        if (directory.exists()) {
            deleteFilesInDir(directory);
        } else {
            directory.mkdirs();
        }


        voiceRipple = (VoiceRippleView) speechView.findViewById(R.id.voice_ripple_view);
        voiceRipple.setRecordingListener(new RecordingListener() {
            @Override
            public void onRecordingStopped() {
                Log.d(TAG, "onRecordingStopped()");

                ctimer.cancel();
                ctimer.onFinish();


                if (playButton.getVisibility() == View.GONE) {
                    playButton.setVisibility(View.VISIBLE);
                    player_duration_tv.setVisibility(View.VISIBLE);
                }
                if (submitBtn.getVisibility() == View.GONE) {
                    submitBtn.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onRecordingStarted() {
                Log.d(TAG, "onRecordingStarted()");
                timer.setVisibility(View.VISIBLE);

                if (playButton.getVisibility() == View.VISIBLE) {
                    playButton.setVisibility(View.GONE);
                    player_duration_tv.setVisibility(View.GONE);
                }

                if (submitBtn.getVisibility() == View.VISIBLE) {
                    submitBtn.setVisibility(View.GONE);
                }


            }
        });

        // set view related settings for ripple view
        voiceRipple.setRippleSampleRate(Rate.HIGH);
        voiceRipple.setRippleDecayRate(Rate.HIGH);
        voiceRipple.setBackgroundRippleRatio(1.4);

        // set recorder related settings for ripple view

        voiceRipple.setMediaRecorder(new MediaRecorder());
        voiceRipple.setOutputFile(filePath().getAbsolutePath());
        voiceRipple.setAudioSource(MediaRecorder.AudioSource.MIC);
        voiceRipple.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        voiceRipple.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        // set inner icon
        voiceRipple.setRecordDrawable(ContextCompat.getDrawable(getContext(), R.drawable.record), ContextCompat.getDrawable(getContext(), R.drawable.recording));
        voiceRipple.setIconSize(50);

        voiceRipple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (voiceRipple.isRecording()) {
                    ctimer.cancel();
                    ctimer.onFinish();
                    voiceRipple.stopRecording();
                } else {
                    ctimer.start();
                    voiceRipple.startRecording();
                }
            }
        });

        currentRenderer = new TimerCircleRippleRenderer(getDefaultRipplePaint(), getDefaultRippleBackgroundPaint(),
                getButtonPaint(), getArcPaint(), 12000.0, 0.0);
        if (currentRenderer instanceof TimerCircleRippleRenderer) {
            ((TimerCircleRippleRenderer) currentRenderer).setStrokeWidth(20);
        }

        voiceRipple.setRenderer(currentRenderer);


        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (directory != null && speechContentList.get(position).getFilePath() != null) {

                    if (!player.isPlaying()) {
                        try {
                            player.reset();
                            player.setDataSource(speechContentList.get(position).getFilePath());
                            player.prepare();
                            player.start();
                            long duration = player.getDuration();
                            System.out.println(duration + "");
                        } catch (IOException e) {
                            Log.e(TAG, "prepare() failed");
                        }

                    } else {
                        player.stop();

                    }
                }
            }
        });
        return speechView;
    }

    public String getDuration() {
        MediaPlayer mp = MediaPlayer.create(getActivity(), Uri.parse(speechContentList.get(position).getFilePath()));
        int duration = mp.getDuration();
//        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);

        return "" + (int) ((duration / (1000 * 60)) % 60);
    }

    private static String getDuration(String path) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(path);
        String durationStr = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        return formateMilliSeccond(Long.parseLong(durationStr));
    }

    public static String formateMilliSeccond(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);

        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        //      return  String.format("%02d Min, %02d Sec",
        //                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
        //                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
        //                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));

        // return timer string
        return finalTimerString;
    }

    public void clearRow(final int position1) {
        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Hey!!!")
                .setContentText("Are you sure you want to skip?")
                .setConfirmText("YES")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        sharedPreferencesData = new SharedPreferencesData();

                        speechContentList.remove(position1);
                        sharedPreferencesData.clearDataSharedPref(Constant.SPEECH_CONTENT);

                        for (SpeechContent content : speechContentList) {
                            sharedPreferencesData.addSharedPrefSpeechContent(content, Constant.SPEECH_CONTENT);
                        }

                        Intent intent = new Intent(getActivity().getApplication(), SpeechActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        onStop();
                        onDestroy();

                    }
                })
                .show();
    }


    private boolean deleteFilesInDir(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }

            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    files[i].delete();
                }
            }
        }
        return true;
    }


    private Paint getArcPaint() {
        Paint paint = new Paint();
        paint.setColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));
        paint.setColor(ContextCompat.getColor(getContext(), R.color.primary));
        paint.setStrokeWidth(20);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.SQUARE);
        paint.setStyle(Paint.Style.STROKE);
        return paint;
    }

    private Paint getDefaultRipplePaint() {
        Paint ripplePaint = new Paint();
        ripplePaint.setStyle(Paint.Style.FILL);
        ripplePaint.setColor(ContextCompat.getColor(getContext(), R.color.derk_red));
        ripplePaint.setColor(ContextCompat.getColor(getContext(), R.color.primary_dark));
        ripplePaint.setAntiAlias(true);

        return ripplePaint;
    }

    private Paint getDefaultRippleBackgroundPaint() {
        Paint rippleBackgroundPaint = new Paint();
        rippleBackgroundPaint.setStyle(Paint.Style.FILL);
        rippleBackgroundPaint.setColor((ContextCompat.getColor(getContext(), R.color.light_red) & 0x00FFFFFF) | 0x40000000);
        rippleBackgroundPaint.setColor((ContextCompat.getColor(getContext(), R.color.light_blue) & 0x00FFFFFF) | 0x40000000);
        rippleBackgroundPaint.setAntiAlias(true);

        return rippleBackgroundPaint;
    }

    private Paint getButtonPaint() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(getResources().getColor(R.color.derk_red));
        paint.setColor(getResources().getColor(R.color.primary_dark));
        paint.setStyle(Paint.Style.FILL);
        return paint;
    }


    @Override
    public void onStop() {
        super.onStop();
        try {

            if (voiceRipple.isRecording()) {
                voiceRipple.stopRecording();
            }
            voiceRipple.onStop();
        } catch (IllegalStateException e) {
            Log.e(TAG, "onStop(): ", e);
        }


        if (player != null) {
            player.release();
            player = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (voiceRipple.isRecording())
            voiceRipple.onDestroy();
    }


    public File filePath() {
        long currentTimeMillis = System.currentTimeMillis();
        File audioFile = new File(directory + "/_" + speechContentList.get(position).getId() + "_audio_" + currentTimeMillis + "_.mp3");
        speechContentList.get(position).setFilePath(audioFile.getAbsolutePath());

        return audioFile;
    }

    public List<SpeechContent> getListsItem() {

        return speechContentList;
    }

    public void SubmitData() {

        AsyncHttpClient client = new AsyncHttpClient();
        File myFile = new File(speechContentList.get(position).getFilePath());
        RequestParams params = new RequestParams();

        try {
//            params.put("content", speechContentList.get(position).getContent());

            String duration = getDuration(speechContentList.get(position).getFilePath());
            params.put("audio_file", myFile);
            params.put("audio_length", duration);

            System.out.println("audioLangth:"+duration);

        } catch (FileNotFoundException e) {
        }

        client.post(Constant.speech_update + speechContentList.get(position).getId() + "?api_token=" + token, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                pogressDialiog.dismiss();
                try {
                    if (response.getString("code").equals("200")) {

                        CommonActivity commonActivity = new CommonActivity();
                        commonActivity.points(Constant.point_speech);

                        sharedPreferencesData = new SharedPreferencesData();

                        speechContentList.remove(position);
                        sharedPreferencesData.clearDataSharedPref(Constant.SPEECH_CONTENT);
                        for (SpeechContent content : speechContentList) {

                            sharedPreferencesData.addSharedPrefSpeechContent(content, Constant.SPEECH_CONTENT);
                        }

                        //                    final SpeechActivity demoActivity = new SpeechActivity();
                        //                                        getActivity().runOnUiThread(new Runnable() {
                        //                        @Override
                        //                        public void run() {
                        ////                            demoActivity.Update(speechContentList,position);
                        //
                        //                        }
                        //                    });

                        new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Success!!!")
                                .setContentText("Data Submitted")
                                .setConfirmText("OK")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();

                                        //                                    SpeechActivity demoActivity  = new SpeechActivity();\
                                        Intent intent = new Intent(getActivity().getApplication(), SpeechActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        onFinish();

                                    }
                                })
                                .show();
                    } else if (response.getString("code").equals("405")) {

                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Token Mismatched")
                                .setConfirmText("Log in again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        try {
                                            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name
                                            snappydb.del(Constant.USER_MODEL);
                                            SharedPreferencesData sharedPreferencesData = new SharedPreferencesData();
                                            sharedPreferencesData.clearDataSharedPref(Constant.SPEECH_CONTENT);
                                            sharedPreferencesData.clearDataSharedPref(Constant.SENTIMENT_CONTENT);

                                        } catch (SnappydbException e) {
                                            e.printStackTrace();
                                        }

                                        Intent intent = new Intent(getActivity().getApplication(), LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        onFinish();
                                    }
                                })
                                .show();

                    } else {
                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Sorry, something went wrong.")
                                .setConfirmText("try again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                pogressDialiog.dismiss();
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Opss!!!")
                        .setContentText("Sorry, something went wrong.")
                        .setConfirmText("try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }

        });

    }


}
