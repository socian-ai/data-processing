package ai.socian.dataprocessing.activity.sentiment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hanks.library.AnimateCheckBox;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ai.socian.dataprocessing.activity.TermsAndConditionsActivity;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.activity.LoginActivity;
import ai.socian.dataprocessing.activity.SentimentActivity;
import ai.socian.dataprocessing.model.SentimentContent;
import ai.socian.dataprocessing.model.User;
import ai.socian.dataprocessing.util.ApplicationSingleton;
import ai.socian.dataprocessing.util.CommonActivity;
import ai.socian.dataprocessing.util.Constant;
import ai.socian.dataprocessing.util.SharedPreferencesData;

public class SentimentShowFragment extends Fragment {
    private static final String TAG = "SentimentTagActivity";
    SharedPreferencesData sharedPreferencesData;
    private String token;
    DB snappydb;
    SweetAlertDialog pogressDialiog;
    Button submitBtn, skipBtn;
    int position = 0;
    List<SentimentContent> sentimentContentList = new ArrayList<>();
    TextView contentID, contentText,terms_tv;

    AnimateCheckBox positive, negative, neutral, question, slang, other;

//    positive = Objective
//    negative =  Strongly Positive
//    neutral =  Weakly Positive
//    question =  Neutral
//    slang =  Weakly Negative
//    other =  Strongly Negative


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        position = getArguments().getInt("position");

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View speechView = inflater.inflate(R.layout.fragment_sentiment_show, container, false);

        sentimentContentList = new ArrayList<>();

        CommonActivity commonActivity = new CommonActivity();

        sentimentContentList = commonActivity.getSentimentListStorageData();

        try {
            snappydb = DBFactory.open(getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);
            token = model.getToken();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        contentID = (TextView) speechView.findViewById(R.id.content_id);
        contentText = (TextView) speechView.findViewById(R.id.content_text);
        submitBtn = (Button) speechView.findViewById(R.id.submit_button);
        skipBtn = (Button) speechView.findViewById(R.id.skip_button);

        positive = (AnimateCheckBox) speechView.findViewById(R.id.positive_chk);
        negative = (AnimateCheckBox) speechView.findViewById(R.id.negative_chk);
        neutral = (AnimateCheckBox) speechView.findViewById(R.id.neutral_chk);
        question = (AnimateCheckBox) speechView.findViewById(R.id.question_chk);
        slang = (AnimateCheckBox) speechView.findViewById(R.id.slang_chk);
        other = (AnimateCheckBox) speechView.findViewById(R.id.other_chk);

        terms_tv = (TextView) speechView.findViewById(R.id.terms_tv);

//        terms_tv.setText(" ");
        setUnderLineText(terms_tv, "Rules & Conditions");

        terms_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TermsAndConditionsActivity.class));
                getActivity().overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });


        contentID.setText("ID : " + sentimentContentList.get(position).getId() + "");
        contentText.setText(sentimentContentList.get(position).getContent() + "");

        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearRow(position);
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pogressDialiog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
                pogressDialiog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
                pogressDialiog.setTitleText("Loading");
                pogressDialiog.setCancelable(false);
                pogressDialiog.show();


                if (!checkbtnValidate()) {
                    pogressDialiog.dismiss();
                    new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("hey!!!")
                            .setContentText("Please, Select the correct answer")
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }
                int checked = chackBoxUpdate(positive) + chackBoxUpdate(negative) + chackBoxUpdate(neutral) + chackBoxUpdate(question) + chackBoxUpdate(slang) + chackBoxUpdate(other);
                Log.d("TOTAL_CHECKED","TOTAL_CHECKED::"+checked);
                if (checked > 1){
                    pogressDialiog.dismiss();
                    Toast.makeText(getActivity(), "Please Select One Option", Toast.LENGTH_SHORT).show();
                    return;
                }
                SubmitData();
            }
        });


        return speechView;
    }

    public void setUnderLineText(TextView tv, String textToUnderLine) {
        String tvt = tv.getText().toString();
        int ofe = tvt.indexOf(textToUnderLine, 0);

        UnderlineSpan underlineSpan = new UnderlineSpan();
        SpannableString wordToSpan = new SpannableString(tv.getText());
        for (int ofs = 0; ofs < tvt.length() && ofe != -1; ofs = ofe + 1) {
            ofe = tvt.indexOf(textToUnderLine, ofs);
            if (ofe == -1)
                break;
            else {
                wordToSpan.setSpan(underlineSpan, ofe, ofe + textToUnderLine.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv.setText(wordToSpan, TextView.BufferType.SPANNABLE);
            }
        }
    }


    public void clearRow(final int position1) {


        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Hey!!!")
                .setContentText("Are you sure you want to skip?")
                .setConfirmText("YES")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        sharedPreferencesData = new SharedPreferencesData();

                        sentimentContentList.remove(position1);
                        sharedPreferencesData.clearDataSharedPref(Constant.SENTIMENT_CONTENT);
                        for (SentimentContent content : sentimentContentList) {

                            sharedPreferencesData.addSharedPrefSentimentContent(content, Constant.SENTIMENT_CONTENT);
                        }


//                                    SpeechActivity demoActivity  = new SpeechActivity();\
                        Intent intent = new Intent(getActivity().getApplication(), SentimentActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        onDestroy();

                    }
                })
                .show();
    }


    @Override
    public void onStop() {
        super.onStop();
//        try {

//            if (voiceRipple.isRecording()) {
//                voiceRipple.stopRecording();
//            }
//            voiceRipple.onStop();
//        } catch (IllegalStateException e) {
//            Log.e(TAG, "onStop(): ", e);
//        }


//        if (player != null) {
//            player.release();
//            player = null;
//        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (voiceRipple.isRecording())
//            voiceRipple.onDestroy();
    }

    private int chackBoxUpdate(AnimateCheckBox checkBox) {
        int value = 0;
        if (checkBox.isChecked()) {
            value = 1;
        } else {
            value = 0;
        }
        return value;
    }


    public boolean checkbtnValidate() {
        if (chackBoxUpdate(positive) == 0 && chackBoxUpdate(negative) == 0 && chackBoxUpdate(neutral) == 0 && chackBoxUpdate(question) == 0 &&
                chackBoxUpdate(slang) == 0 && chackBoxUpdate(other) == 0) {
            return false;
        }
        return true;
    }


    public void SubmitData() {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();


//    positive = Objective
//    negative =  Strongly Positive
//    neutral =  Weakly Positive
//    question =  Neutral
//    slang =  Weakly Negative
//    other =  Strongly Negative
//
//
//
//
//
//
        try {
            params.put("objective", chackBoxUpdate(positive));
            params.put("strongly_positive", chackBoxUpdate(negative));
            params.put("weakly_positive", chackBoxUpdate(neutral));
            params.put("neutral", chackBoxUpdate(question));
            params.put("weakly_negative", chackBoxUpdate(slang));
            params.put("strongly_negative", chackBoxUpdate(other));


        } catch (Exception e) {
        }

        client.post(Constant.sentiment_update + sentimentContentList.get(position).getId() + "?api_token=" + token, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                pogressDialiog.dismiss();
                try {
                    if (response.getString("code").equals("200")) {

                        CommonActivity commonActivity = new CommonActivity();
                        commonActivity.points(Constant.point_sentiment);

                        sharedPreferencesData = new SharedPreferencesData();

                        sentimentContentList.remove(position);
                        sharedPreferencesData.clearDataSharedPref(Constant.SENTIMENT_CONTENT);
                        for (SentimentContent content : sentimentContentList) {

                            sharedPreferencesData.addSharedPrefSentimentContent(content, Constant.SENTIMENT_CONTENT);
                        }


                        new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Success!!!")
                                .setContentText("Data Submitted")
                                .setConfirmText("OK")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();

                                        //                                    SpeechActivity demoActivity  = new SpeechActivity();\
                                        Intent intent = new Intent(getActivity().getApplication(), SentimentActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        onFinish();

                                    }
                                })
                                .show();
                    } else if (response.getString("code").equals("405")) {

                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Token Mismatched")
                                .setConfirmText("Log in again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        try {
                                            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name
                                            snappydb.del(Constant.USER_MODEL);
                                            SharedPreferencesData sharedPreferencesData = new SharedPreferencesData();
                                            sharedPreferencesData.clearDataSharedPref(Constant.SPEECH_CONTENT);
                                            sharedPreferencesData.clearDataSharedPref(Constant.SENTIMENT_CONTENT);

                                        } catch (SnappydbException e) {
                                            e.printStackTrace();
                                        }

                                        Intent intent = new Intent(getActivity().getApplication(), LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        onFinish();
                                    }
                                })
                                .show();

                    } else {
                        pogressDialiog.dismiss();
                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Sorry, something went wrong.")
                                .setConfirmText("try again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

//            @Override
//            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//
//                pogressDialiog.dismiss();
//                if (statusCode == 200) {
//
//                    CommonActivity commonActivity = new CommonActivity();
//                    commonActivity.points(Constant.point_sentiment);
//
//                    sharedPreferencesData = new SharedPreferencesData();
//
//                    sentimentContentList.remove(position);
//                    sharedPreferencesData.clearDataSharedPref(Constant.SENTIMENT_CONTENT);
//                    for (SentimentContent content : sentimentContentList) {
//
//                        sharedPreferencesData.addSharedPrefSentimentContent(content, Constant.SENTIMENT_CONTENT);
//                    }
//
//
//                    new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
//                            .setTitleText("Success!!!")
//                            .setContentText("Data Submitted")
//                            .setConfirmText("OK")
//                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                @Override
//                                public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                    sweetAlertDialog.dismiss();
//
////                                    SpeechActivity demoActivity  = new SpeechActivity();\
//                                    Intent intent = new Intent(getActivity().getApplication(), SentimentActivity.class);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                    startActivity(intent);
//                                    onFinish();
//
//                                }
//                            })
//                            .show();
//                } else {
//                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
//                            .setTitleText("Opss!!!")
//                            .setContentText("Sorry, something went wrong.")
//                            .setConfirmText("try again")
//                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                @Override
//                                public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                    sweetAlertDialog.dismiss();
//                                }
//                            })
//                            .show();
//                }
//            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                pogressDialiog.dismiss();
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Opss!!!")
                        .setContentText("Sorry, something went wrong.")
                        .setConfirmText("try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                pogressDialiog.dismiss();
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Opss!!!")
                        .setContentText("Sorry, something went wrong.")
                        .setConfirmText("try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                pogressDialiog.dismiss();
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Opss!!!")
                        .setContentText("Sorry, something went wrong.")
                        .setConfirmText("try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }

            //            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//
//            }
        });

    }


}
