package ai.socian.dataprocessing.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.model.User;
import ai.socian.dataprocessing.util.ApplicationSingleton;
import ai.socian.dataprocessing.util.CommonActivity;
import ai.socian.dataprocessing.util.Constant;

public class SignUpConfirmActivity extends CommonActivity {

    @BindView(R.id.send_btn)
    AppCompatButton send_btn;

    @BindView(R.id.back_button)
    AppCompatButton back_button;

    @BindView(R.id.pin_number)
    TextInputEditText pin_number;

    SweetAlertDialog pogressDialog;
    User userModel;
    DB snappydb;
    String phoneNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_confirm);
        ButterKnife.bind(this);



        phoneNumber = getIntent().getStringExtra("number");

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpConfirmActivity.this,SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });

        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setConfirmation();
            }
        });

    }

    public void setConfirmation(){


        pogressDialog = new SweetAlertDialog(SignUpConfirmActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pogressDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
        pogressDialog.setTitleText("Loading");
        pogressDialog.setCancelable(false);
        pogressDialog.show();


        if(!isOnline()){
            pogressDialog.dismiss();

            new SweetAlertDialog(SignUpConfirmActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Opss!!!")
                    .setContentText("Please Active Your Internet Connection")
                    .setConfirmText("Okay!")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();
            return;
        }

        if (pin_number.getText().toString().isEmpty()){
            pogressDialog.dismiss();
            pin_number.setError("You didn't entered any pin number");
            return;
        }

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("mobile_number",phoneNumber);
        params.put("verification_key", pin_number.getText().toString());

        client.post(Constant.number_verification, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                try {
                    if (response.getString("code").equals("200")) {


                        try {
                            JSONObject user = new JSONObject();
                            user = response.getJSONObject("user");
                            int id = user.getInt("id");
                            String name = user.getString("name");
                            String email = user.getString("email");
                            String api_token = user.getString("api_token");
                            String NID = user.getString("NID");
                            String mobile_number = user.getString("mobile_number");
                            String bikash_number = user.getString("bikash_number");
                            String rocket_number = user.getString("rocket_number");
                            String gender = user.getString("gender");
                            String birth_day = user.getString("birth_day");
                            String created_at = user.getString("created_at");
                            String updated_at = user.getString("updated_at");

                            userModel = new User();
                            userModel.setUserID(id);
                            userModel.setName(name);
                            userModel.setEmail(email);
                            userModel.setToken(api_token);
                            userModel.setNID(NID);
                            userModel.setMobileNumber(mobile_number);
                            userModel.setBkashNumber(bikash_number);
                            userModel.setRocketNumber(rocket_number);
                            userModel.setGender(gender);
                            userModel.setBirthday(birth_day);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {
                            snappydb = DBFactory.open(ApplicationSingleton.getContext());
                            snappydb.put(Constant.USER_MODEL, userModel);


                            User model = new User();
                            model = snappydb.get(Constant.USER_MODEL, User.class);

                            String token = model.getToken();


                            Log.i(TAG, "TOKEN:" + token);

                            snappydb.close();

                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }

                        pogressDialog.dismiss();
                        new SweetAlertDialog(SignUpConfirmActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Account successfully created!!!")
                                .setContentText("You logged in successfully")
                                .setConfirmText("GO")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Intent intent = new Intent(SignUpConfirmActivity.this, MainActivity.class);
                                        sweetAlertDialog.dismiss();
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                                .show();
                    } else {

                        pogressDialog.dismiss();
                        new SweetAlertDialog(SignUpConfirmActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Sorry, something went wrong.")
                                .setConfirmText("try again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                pogressDialog.dismiss();
                new SweetAlertDialog(SignUpConfirmActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Opss!!!")
                        .setContentText("Sorry, something went wrong.")
                        .setConfirmText("try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();

            }
        });




    }
}
