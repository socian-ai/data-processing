package ai.socian.dataprocessing.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.model.User;
import ai.socian.dataprocessing.services.GetIfSpeechContentAvailable;
import ai.socian.dataprocessing.util.ApplicationSingleton;
import ai.socian.dataprocessing.util.CommonActivity;
import ai.socian.dataprocessing.util.Constant;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import io.fabric.sdk.android.Fabric;

public class PrivacyPolicyActivity extends CommonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_privacy_policy);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Privacy Policy");

        TextView privacy_tv = (TextView) findViewById(R.id.privacy_tv);

        String htmlText = "<h1>Privacy Policy</h1> <div> <p>Socian Ltd./Socian AI respects your privacy. " +
                "Socian and its affiliates (\"Socian\" or the&nbsp;“Company”&nbsp;) (Licensed under the Govt. of Bangladesh) " +
                "offer Artificial Intelligence based services to different level of users. More of Socian can be found at " +
                "www.socian.ai&nbsp;&nbsp;(the \"Website\") collectively referred to as the “Services\").</p> <p>" +
                "This Privacy Policy describes:-</p> <p>● &nbsp; &nbsp; &nbsp;The information that Socian collects from users;" +
                "</p> <p>● &nbsp; &nbsp; &nbsp;How that information is used by Socian; and</p> <p>● &nbsp; &nbsp; &nbsp;" +
                "The rights and options available to users.</p> <p>" +
                "This Privacy Policy is incorporated as part of the Socian&nbsp;Terms of Use&nbsp;&nbsp;(the \"Terms and Conditions\" or " +
                "the \"Terms\").</p> <p>Your use of the Services is subject to the Terms and Conditions and this Privacy Policy " +
                "and indicates your consent to them.</p> <p>&nbsp;</p> <p>" +
                "</p> <p>&nbsp;</p> <p>In this Privacy Policy, the term \"personal information\" means any information collected by us or " +
                "provided by you that, with reasonable efforts, may be used to identify you, which may include first and last name, home address," +
                " email address and, in certain circumstances, your phone number, NID and some other information.</p> <p>" +
                "Contact Socian / Abuse reports</p> <p>&nbsp;</p> <p>If you believe that your privacy has been compromised by any person, " +
                "in the course of using the Services, then please contact Socian at:&nbsp;contact@socian.ai. " +
                "In this App, the main objective is collecting your Voice Sample and Text sample to use for the improvement of Socian's " +
                "Artificial Intelligence. We collect your voice data and will use it for Speech to Text and Text to Speech. We collect your Sentiments (Tagging) in order to use them for Sentiment Analysis." +
                "All the things that you do, you are paid for them. All the Data are property of Socian Ltd and you are being paid for that." +
                "Socian can use these data the way is appropriate.</p> <p>&nbsp;</p> <p>" +
                "You may also send Socian other requests, responses, questions and complaints.</p> <p>&nbsp;</p> <p><strong>" +
                "Information that is being collected</strong></p> <p>Socian allows you to use the Services by registering your phone number, email and NID to " +
                "the application.&nbsp; Socian will link all of your information with your account and a unique identifier " +
                "generated by Socian in accordance with this Privacy Policy.</p> <p>&nbsp;</p> <p>The categories of " +
                "information collected by Socian will include all or just some of the following, depending on how you " +
                "choose to use the Services.</p> <p>&nbsp;</p> <p><strong>Whenever you use the Services, " +
                "Socian&nbsp;will&nbsp;collect:</strong></p> <p>● Your Voice Data corresponding with the available text. " +
                "</p>  <p>● Your Sentiment Data corresponding the sentences </p> " +
                "<p>● Information about your use of the Services. &nbsp;See “Meta-data that we collect”, below;</p> <p>" +
                "If you choose to use the Services in a manner detailed below, Socian&nbsp;may&nbsp;also collect:</p> <p>" +
                "● Information that you choose to share with Socian, including your phone number, a username you have chosen, " +
                "reports associated with your username, National ID (To verify you are the real user), Email ID, " +
                "etc. Bear in mind that false, incorrect, or out-dated information may impair Socian's ability to provide you with the Services " +
                "and to contact you when necessary. Socian will explicitly indicate the fields for mandatory completion. " +
                "If you do not enter the requisite data in these fields, you may not be able to set up a username; and/ or</p> " +
                "<p>● Information shared from your social network accounts. See “Integration with social networks”, below.</p> " +
                "<p>Please note that when you first install the Application on your mobile device, Socian will set up an account associated " +
                "with that mobile device (the&nbsp;“Account”).&nbsp;&nbsp;&nbsp;Socian will collect and use your information, " +
                "in accordance with this Privacy Policy, whenever you activate the Application on that mobile device. &nbsp;This use includes " +
                "linking your information with your Account. &nbsp;This is the case irrespective of whether or not you choose to create a " +
                "Socian Phone No (Unique Identifier) and password in the Application.</p> <p>" +
                "If you&nbsp;do set up a Phone and password in the Application:</p> <p>● Socian may link all of your information with " +
                "your Account&nbsp;and that particular user. This includes information received from all mobile devices running the " +
                "Application on which you have chosen to sign in using your username and password;</p> <p>● " +
                "With the exception described in the bullet below, your Account and information associated with your Account will be " +
                "visible only to Socian; and</p> " +
                " <p>&nbsp;</p> <p><strong>Usage of information</strong></p> <p>Socian may use the data collected from " +
                "you for its research and if needed, share or sell these data anonymously. " +
                "<p>● &nbsp; &nbsp; To&nbsp;&nbsp;improve the experience of other users. " +
                "<p>● &nbsp; &nbsp; &nbsp;To provide you with support and handle requests and complaints;</p> " +
                "<p>● &nbsp; &nbsp; &nbsp;To&nbsp;&nbsp;display or send to you marketing and advertising material when you are using the Application, in accordance with the section titled 'Advertising Campaigns' under this Privacy Policy. " +
                "For example, Socian may use your information to display advertisements for businesses in your close vicinity;</p> <p>● &nbsp; &nbsp; &nbsp;&nbsp;Subject to your prior indication of consent, we may also use the email address that you provide when setting up a username to send you marketing material. Socian will not knowingly share your personal information with any advertisers, without your explicit consent and as allowed for under this Privacy Policy;</p> " +
                "<p>● &nbsp; &nbsp; &nbsp;&nbsp;to&nbsp;create aggregated and/ or anonymous data (where such data does not enable the identification of a specific user). &nbsp;See “How does Socian use aggregated information”, below;</p> <p>● &nbsp; &nbsp; &nbsp;To conduct surveys and questionnaires;</p> <p>● &nbsp; &nbsp; &nbsp;To share your submissions with other users of the Services and to facilitate communication between you, Socian and other users;</p> <p>● &nbsp; &nbsp; &nbsp;To enforce the Terms of Use;</p> <p>● &nbsp; &nbsp; &nbsp;To contact you when Socian believes it to be necessary;</p> <p>● &nbsp; &nbsp; &nbsp;To comply with any applicable law and assist law enforcement agencies under any applicable law, when Socian has a good faith belief that Socian’s cooperation with the law enforcement agencies is legally mandated or meets the applicable legal standards and procedures;</p> <p>● &nbsp; &nbsp; &nbsp;To prevent fraud, misappropriation, infringements, identity theft and other illegal activities and misuse of the Services;</p> <p>● &nbsp; &nbsp; &nbsp;To handle breakdowns and malfunctions;</p> <p>● &nbsp; &nbsp; &nbsp;To take any action in any case of dispute, or legal proceeding of any kind between you and the Service, or between you and other users or third parties with respect to, or in relation with the Service;</p> <p>● &nbsp; &nbsp; &nbsp;&nbsp;For purposes provided under this Privacy Policy and the Terms of Use.</p> <p>&nbsp;</p> <p>Information may be collected when Socian exchanges communications with you, for example, if you submit a request, contact Socian's support team, or report a violation to the abuse team.</p> <p>&nbsp;</p> <p><strong>Personal information that you share</strong></p> <p>Socian collects and processes personal data on a voluntary basis and it is not in the business of selling your personal data to third parties. Personal data may, however, occasionally be disclosed in accordance with applicable legislation and this Privacy Policy. Additionally, Socian may disclose personal data to its parent companies and its subsidiaries in accordance with this Privacy Policy.</p> <p>&nbsp;</p> <p>Socian may hire agents and contractors to collect and process personal data on Socian’s behalf and in such cases such agents and contractors will be instructed to comply with our Privacy Policy and to use personal data only for the purposes for which the third party has been engaged by Socian. These agents and contractors may not use your personal data for their own marketing purposes. " +
                "Socian may use third party service providers such as credit card processors, e-mail service providers, shipping agents, data analyzers, and business intelligence providers. Socian has the right to share your personal data as necessary for the aforementioned third parties to provide their services for Socian. Socian is not liable for the acts and omissions of these third parties, except as provided by mandatory law.</p> <p>&nbsp;</p> <p>Socian may disclose your personal data to third parties as required by law enforcement or other government officials in connection with an investigation of fraud, intellectual property infringements, or other activity that is illegal or may expose you or Socian to legal liability. Socian may also disclose your personal data to third parties when Socian has a reason to believe that a disclosure is necessary to address potential or actual injury or interference with Socian’s rights, property, operations, users, or others who may be harmed or may suffer loss or damage, or Socian believes that such disclosure is necessary to protect Socian’s rights, combat fraud and/or comply with a judicial proceeding, court order, or legal process served on Socian. To the extent permitted by applicable law, Socian will make reasonable efforts to notify you of such disclosure through Socian’s website or in another reasonable manner.</p> <p>&nbsp;</p> <p><strong>Safeguards</strong></p> <p>Socian follows generally accepted industry standards and maintains reasonable safeguards to attempt to ensure the security, integrity, and privacy of the information in Socian’s possession. " +
                "Only those persons with a need to process your personal data in connection with the fulfillment of their tasks in accordance with the purposes of this Privacy Policy and for the purposes of performing technical maintenance, have access to your personal data in Socian’s possession. Personal data collected by Socian is stored in secure operating environments that are not available to the public. To prevent unauthorized on-line access to personal data, Socian maintains personal data behind a firewall-protected server. " +
                "However, no system can be 100% secure and there is the possibility that despite Socian’s reasonable efforts, there could be unauthorized access to your personal data. By using the Services, you assume this risk.</p> <p>&nbsp;</p> <p><strong>Meta-data that we collect</strong></p> <p>Socian collects information about the use of the Services. &nbsp;" +
                "For example, Socian may record the frequency and scope of your use of the Services, the duration of your sessions, advertisements that you view or click on etc.</p> <p>&nbsp;</p> <p><strong>Sharing information with others</strong></p> <p>Socian does not sell, rent or lease your personal information to third parties.</p> <p>Socian will not share your personal information with others, without your consent, except for the following purposes and to the extent necessary in Socian's good-faith discretion:</p> <p>" +
                "● As necessary for the operation of the Services. For example, making public News you report</p> <p>● If Socian reasonably believes that you have breached the Terms of Use, or abused your rights to use the Services, or performed any act or omission that Socian reasonably believes to be violating any applicable law, rules, or regulations. Socian may share your information in these cases, with law enforcement agencies and other competent authorities and with any third party as may be required to handle any result of your wrongdoing;</p> <p>● If Socian is required, or reasonably believes that it is required by law to share or disclose your information;</p> " +
                "<p>● In any case of dispute, or legal proceeding of any kind between you and Socian, or between you and other users with respect to, or in relation with the Services;</p> <p>● In any case where Socian reasonably believes that sharing information is necessary to prevent imminent physical damage or damage to property;</p> <p>● If Socian organizes the operation of the Services within a different framework, or through another legal structure or entity, or if Socian is acquired by, or merged into or with another entity, or if Socian enters bankruptcy, provided however, that those entities agree to be bound by the provisions of this Privacy Policy, with respective changes taken into consideration;</p> <p>" +
                "● To collect, hold and manage your personal information through cloud based or hosting services or a third party or a party affiliated or connected to Socian, as reasonable for business purposes, which may be located in countries outside of your jurisdiction, including but not limited to the United States of America.</p> " +
                "<p>● Socian may also share personal information with companies or organizations connected or affiliated with Socian, such as subsidiaries, sister-companies and parent companies. Personal information may also be shared with Socian's other partners and service providers to process it for us, based on our instructions and in compliance with this policy and any other appropriate confidentiality and security measures.</p> <p>&nbsp;</p> <p><strong>Advertising campaigns</strong></p> <p>Socian may permit, solicit or contract certain other companies to conduct advertising campaigns on the Services.</p> <p>When you visit a third party website from the Application, such third party may use cookies on your computer. The third party’s use of cookies is subject to their own privacy policies and not to this Privacy Policy. If you wish to study the privacy policies of those third parties you should visit their services, or contact them directly.</p> <p>The Service includes an internal messaging system which provides you the ability to receive marketing messages from us. You hereby agree that we may use the internal messaging system for the purpose of informing you regarding products or services, which may interest you.</p> <p>&nbsp;</p> <p>Please note however that other Socian users are not allowed to use your contact details or our internal messaging account for commercial advertising purposes. " +
                "If you have encountered any user generated advertising material, please inform us through \"Email\" . Socian does not accept any liability for any loss, damage, cost or expense that you may suffer or incur as a result of or in connection with any user's advertising material.</p> <p>It is clarified that we are not responsible for the content of said advertisements and the products delivered or services rendered thereby by third parties and you irrevocably and unconditionally agree that we shall not be held responsible or liable in connection thereof.</p> " +
                "<p>&nbsp;</p> <p><strong>Information Security</strong></p> <p>Socian considers information security to be a top priority. Socian implements systems, applications and procedures to secure your personal information, to minimize the risks of theft, damage, loss of information, or unauthorized access or use of information. However, these measures are unable to provide absolute assurance. " +
                "Therefore, although Socian takes great efforts to protect your personal information, Socian cannot guarantee and you cannot reasonably expect that Socian's&nbsp;databases will be immune from any wrongdoings, malfunctions, unlawful interceptions or access, or other kinds of abuse and misuse.</p> <p>&nbsp;</p> " +
                "<p><strong>Changes to this Privacy Policy</strong></p> <p>Socian may from time to time change the terms of this Privacy Policy. You agree to be bound by any of the changes made in the terms of this Privacy Policy. Continuing to use the Services will indicate your acceptance of the amended terms. If you do not agree with any of the amended terms, you must avoid any further use of the Services.</p> </div>";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            privacy_tv.setText(Html.fromHtml(htmlText, Html.FROM_HTML_MODE_COMPACT));
        }

        else {
            privacy_tv.setText(Html.fromHtml(htmlText));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
