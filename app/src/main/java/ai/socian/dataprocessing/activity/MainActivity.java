package ai.socian.dataprocessing.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;
import io.fabric.sdk.android.Fabric;
import ai.socian.dataprocessing.R;
import ai.socian.dataprocessing.model.Points;
import ai.socian.dataprocessing.model.User;
import ai.socian.dataprocessing.services.GetIfSpeechContentAvailable;
import ai.socian.dataprocessing.util.ApplicationSingleton;
import ai.socian.dataprocessing.util.CommonActivity;
import ai.socian.dataprocessing.util.Constant;
import ai.socian.dataprocessing.util.SharedPreferencesData;

public class MainActivity extends CommonActivity {


    private static final String TAG = "MainActivity.java";
    @BindView(R.id.speech_button)
    ImageView speech_button;
    @BindView(R.id.sentiment_button)
    ImageView sentiment_button;
    @BindView(R.id.user_name)
    TextView user_name;
    @BindView(R.id.cardview_sentiment)
    CardView cardview_sentiment;
    @BindView(R.id.cardview_speech)
    CardView cardview_speech;
    @BindView(R.id.pogress_layout)
    LinearLayout pogress_layout;

    @BindView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;


    DB snappydb;
    SharedPreferencesData sharedPreferencesData;
    private GetIfSpeechContentAvailable alarm;
    Button sentiment_point_btn, speech_point_btn;

    private TextView navHeaderTV;
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;
    TextView toolbarText;

    TextView nav_header_tv;
    String name;
    String totalSpeechPoints, totalSpeeechSubmitted, currentSpeechPoints, cashoutSpeechPoints,currentTimeString;
    String totalSentimentPoints, totalSentimentSubmitted, currentSentimentPoints, cashoutSentimentPoints;
    TextView speech_current_points, sentiment_current_points;
    String method = "Bkash";
    LinearLayout root_layout;

    @BindView(R.id.sentiment_btn)
    Button sentiment_btn;

    @BindView(R.id.speech_btn)
    Button speech_btn;

    @BindView(R.id.sentiment_btn1)
    ImageView sentiment_btn1;

    @BindView(R.id.speech_btn1)
    ImageView speech_btn1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        speech_current_points = (TextView) findViewById(R.id.speech_current_points);
        sentiment_current_points = (TextView) findViewById(R.id.sentiment_current_points);

        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            name = model.getName();


            Points speechPoint = new Points();
            speechPoint = snappydb.get(Constant.POINTS_SPEECH_KEY, Points.class);
            totalSpeechPoints = speechPoint.getTotalPoints();
            totalSpeeechSubmitted = speechPoint.getTotalSubmitted();
            currentSpeechPoints = speechPoint.getCurrrentPoints();
            cashoutSpeechPoints = speechPoint.getCashoutPending();
            SharedPreferences prefs = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE);
//            String totalSubmitted = prefs.getString("totalSubmitted", "0");

            SharedPreferences prefsc = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE);
            String totalSubmitted = prefsc.getString("totalSubmitted", "0");

            speech_current_points.setText(totalSubmitted);

            Points sentimentPoint = new Points();
            sentimentPoint = snappydb.get(Constant.POINTS_SENTIMENT_KEY, Points.class);
            totalSentimentPoints = sentimentPoint.getTotalPoints();
            totalSentimentSubmitted = sentimentPoint.getTotalSubmitted();
            currentSentimentPoints = sentimentPoint.getCurrrentPoints();
            cashoutSentimentPoints = sentimentPoint.getCashoutPending();

            sentiment_current_points.setText(totalSentimentSubmitted);


            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        setupDrawerContent(nvDrawer);
        drawerToggle = setupDrawerToggle();
        toolbarText = (TextView) findViewById(R.id.toolbar_text);
        if (toolbarText != null && toolbar != null) {
            toolbarText.setText(getTitle());
            setSupportActionBar(toolbar);
        }

        // Tie DrawerLayout events to the ActionBarToggle
        mDrawer.addDrawerListener(drawerToggle);

        View headerView = nvDrawer.getHeaderView(0);
        nav_header_tv = (TextView) headerView.findViewById(R.id.nav_header_tv);
        nav_header_tv.setText(name + "");


//        loadDataIfnotAvailable();

        sentiment_point_btn = (Button) findViewById(R.id.sentiment_point_btn);
        speech_point_btn = (Button) findViewById(R.id.speech_point_btn);


        sentiment_point_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sentimentPointsDetails();
            }
        });

        speech_point_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentEmotion = new Intent(MainActivity.this, EmotionActivity.class);
                startActivity(intentEmotion);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
//                Toast.makeText(MainActivity.this, "Comming Soon", Toast.LENGTH_SHORT).show();
//                return;
//                speechPointsDetails();
            }
        });

        speech_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speechPointsDetails();

            }
        });


        alarm = new GetIfSpeechContentAvailable();
        startRepeatingTimer();
        setUserName();

        speech_btn.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
//                                              /Toast.makeText(MainActivity.this, "Comming Soon", Toast.LENGTH_SHORT).show();
//                                              return;
                                              Intent intentEmotion = new Intent(MainActivity.this, EmotionActivity.class);
                                              startActivity(intentEmotion);
                                              overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                                          }
                                      }
        );

        sentiment_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SentimentActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        speech_btn1.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {

                                              Intent intentEmotion = new Intent(MainActivity.this, EmotionActivity.class);
                                              startActivity(intentEmotion);
                                              overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
//                                              Intent intent = new Intent(MainActivity.this, SpeechActivity.class);
//                                              startActivity(intent);
//                                              overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                                          }
                                      }
        );

        sentiment_btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SentimentActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);

        animation.setRepeatCount(Animation.INFINITE);
        animation.start();
//        root_layout = (LinearLayout) findViewById(R.id.root_layout);
        speech_button.startAnimation(animation);
        sentiment_button.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(final Animation animation) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        speech_button.startAnimation(animation);
                        sentiment_button.startAnimation(animation);
//                        animation.start();
                    }
                }, 3000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sentimentPointsDetails1();
                speechPointsDetails1();
            }
        });

    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }


    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
//        Fragment fragment = null;
//        Class fragmentClass;
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
//                fragmentClass = FirstFragment.class;
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
//                Toast.makeText(this, "" + menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_Profile:
//                fragmentClass = SecondFragment.class;
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
//                Toast.makeText(this, "" + menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_sentiment:
//                fragmentClass = ThirdFragment.class;

                startActivity(new Intent(MainActivity.this, SentimentActivity.class));
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
//                Toast.makeText(this,""+menuItem.getTitle(),Toast.LENGTH_SHORT).show();
                break;

            case R.id.nav_speech:
//                fragmentClass = ThirdFragment.class;

                startActivity(new Intent(MainActivity.this, EmotionActivity.class));
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
//                Toast.makeText(MainActivity.this, "Comming Soon", Toast.LENGTH_SHORT).show();
//                return;
//                startActivity(new Intent(MainActivity.this, SpeechActivity.class));
//                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
//                Toast.makeText(this,""+menuItem.getTitle(),Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_tearms:
//                fragmentClass = ThirdFragment.class;
                startActivity(new Intent(MainActivity.this, TermsAndConditionsActivity.class));
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
//                Toast.makeText(this, "" + menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_privacy:
//                fragmentClass = ThirdFragment.class;
                startActivity(new Intent(MainActivity.this, PrivacyPolicyActivity.class));
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
//                Toast.makeText(this, "" + menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_log_out:
//                fragmentClass = ThirdFragment.class;
                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("LOG OUT")
                        .setContentText("Are you sure?")
                        .setConfirmText("Yes, Logout!")
                        .setCancelText("NO")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                try {
                                    snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name
                                    snappydb.del(Constant.USER_MODEL);
                                    SharedPreferencesData sharedPreferencesData = new SharedPreferencesData();
                                    sharedPreferencesData.clearDataSharedPref(Constant.SPEECH_CONTENT);
                                    sharedPreferencesData.clearDataSharedPref(Constant.SENTIMENT_CONTENT);

                                } catch (SnappydbException e) {
                                    e.printStackTrace();
                                }

                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .show();
//                Toast.makeText(this,""+menuItem.getTitle(),Toast.LENGTH_SHORT).show();
                break;
            default:
//                fragmentClass = FirstFragment.class;
                Toast.makeText(this, "" + menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        }

        try {
//            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        if (drawerToggle.onOptionsItemSelected(item)) {
//            case android.R.id.home:
//                mDrawer.openDrawer(GravityCompat.START);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadDataIfnotAvailable();
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            Points speechPoint = new Points();
            speechPoint = snappydb.get(Constant.POINTS_SPEECH_KEY, Points.class);
            totalSpeechPoints = speechPoint.getTotalPoints();
            totalSpeeechSubmitted = speechPoint.getTotalSubmitted();
            currentSpeechPoints = speechPoint.getCurrrentPoints();
            cashoutSpeechPoints = speechPoint.getCashoutPending();
            SharedPreferences prefs = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE);
            String totalSubmitted = prefs.getString("totalSubmitted", "0");

            speech_current_points.setText(totalSubmitted);

            Points sentimentPoint = new Points();
            sentimentPoint = snappydb.get(Constant.POINTS_SENTIMENT_KEY, Points.class);
            totalSentimentPoints = sentimentPoint.getTotalPoints();
            totalSentimentSubmitted = sentimentPoint.getTotalSubmitted();
            currentSentimentPoints = sentimentPoint.getCurrrentPoints();
            cashoutSentimentPoints = sentimentPoint.getCashoutPending();

            sentiment_current_points.setText(totalSentimentSubmitted);


            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }


    }

    private void sentimentPointsDetails() {

        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name


            Points sentimentPoint = new Points();
            sentimentPoint = snappydb.get(Constant.POINTS_SENTIMENT_KEY, Points.class);
            totalSentimentPoints = sentimentPoint.getTotalPoints();
            totalSentimentSubmitted = sentimentPoint.getTotalSubmitted();
            currentSentimentPoints = sentimentPoint.getCurrrentPoints();
            cashoutSentimentPoints = sentimentPoint.getCashoutPending();

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        final Dialog dialog = new Dialog(MainActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.points_details_layout);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);


        final TextView totalSubmitted = (TextView) dialog.findViewById(R.id.totalSubmitted);
        final TextView totalPoints = (TextView) dialog.findViewById(R.id.totalPoints);
        final TextView currentPoints = (TextView) dialog.findViewById(R.id.currentPoints);
        final TextView cashoutReq = (TextView) dialog.findViewById(R.id.cashoutReq);
        final TextView title = (TextView) dialog.findViewById(R.id.title);
        Button cancelBtn = (Button) dialog.findViewById(R.id.cancelBtn);
        Button refresh = (Button) dialog.findViewById(R.id.refresh);
        LinearLayout cancel_layout = (LinearLayout) dialog.findViewById(R.id.cancel_layout);
        Button cashoutReqBtn = (Button) dialog.findViewById(R.id.cashoutReqBtn);
        cashoutReqBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Double.parseDouble(currentSentimentPoints) <10){
                    new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Sorry!!!")
                            .setContentText("You can not request at the bottom of 10 points")
                            .setConfirmText("Try Again")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();

                                }
                            })
                            .show();
                    return;
                }
                dialog.dismiss();

                pointRequest(Constant.point_sentiment_request, "Sentiment");
            }
        });

        cancel_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                points(Constant.point_sentiment);

                final SweetAlertDialog pogressDialiog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                pogressDialiog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
                pogressDialiog.setTitleText("Loading");
                pogressDialiog.setCancelable(false);
                pogressDialiog.show();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pogressDialiog.dismiss();
                        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Success!!!")
                                .setContentText("Points have been updated!")
                                .setConfirmText("Oky")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                                        sweetAlertDialog.dismiss();

                                    }
                                })
                                .show();
                        try {

                            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

                            Points sentimentPoint = new Points();
                            sentimentPoint = snappydb.get(Constant.POINTS_SENTIMENT_KEY, Points.class);
                            totalSentimentPoints = sentimentPoint.getTotalPoints();
                            totalSentimentSubmitted = sentimentPoint.getTotalSubmitted();
                            currentSentimentPoints = sentimentPoint.getCurrrentPoints();
                            cashoutSentimentPoints = sentimentPoint.getCashoutPending();

                            snappydb.close();

                            title.setText("Sentiment");
                            totalSubmitted.setText(totalSentimentSubmitted);
                            totalPoints.setText(totalSentimentPoints);
                            currentPoints.setText(currentSentimentPoints);
                            cashoutReq.setText(cashoutSentimentPoints);
                            sentiment_current_points.setText(totalSentimentSubmitted);
                            mSwipeRefreshLayout.setRefreshing(false);


                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }

                    }
                }, 3000);

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        title.setText("Sentiment");
        totalSubmitted.setText(totalSentimentSubmitted);
        totalPoints.setText(totalSentimentPoints);
        currentPoints.setText(currentSentimentPoints);
        cashoutReq.setText(cashoutSentimentPoints);

//        recycler_view =  (RecyclerView) dialog.findViewById(R.id.recycler_view);
//        no_content_tv = (TextView) dialog.findViewById(R.id.no_content_tv);

        dialog.show();

    }

    private void sentimentPointsDetails1() {

        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name
            Points sentimentPoint = new Points();
            sentimentPoint = snappydb.get(Constant.POINTS_SENTIMENT_KEY, Points.class);
            totalSentimentPoints = sentimentPoint.getTotalPoints();
            totalSentimentSubmitted = sentimentPoint.getTotalSubmitted();
            currentSentimentPoints = sentimentPoint.getCurrrentPoints();
            cashoutSentimentPoints = sentimentPoint.getCashoutPending();

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        points(Constant.point_sentiment);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                try {

                    snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

                    Points sentimentPoint = new Points();
                    sentimentPoint = snappydb.get(Constant.POINTS_SENTIMENT_KEY, Points.class);
                    totalSentimentPoints = sentimentPoint.getTotalPoints();
                    totalSentimentSubmitted = sentimentPoint.getTotalSubmitted();
                    currentSentimentPoints = sentimentPoint.getCurrrentPoints();
                    cashoutSentimentPoints = sentimentPoint.getCashoutPending();

                    sentiment_current_points.setText(totalSentimentSubmitted);
                    snappydb.close();


                    mSwipeRefreshLayout.setRefreshing(false);


                } catch (SnappydbException e) {
                    e.printStackTrace();
                }

            }
        }, 3000);


    }


    private void speechPointsDetails() {

        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            Points speechPoint = new Points();
            speechPoint = snappydb.get(Constant.POINTS_SPEECH_KEY, Points.class);
            totalSpeechPoints = speechPoint.getTotalPoints();
            totalSpeeechSubmitted = speechPoint.getTotalSubmitted();
            currentSpeechPoints = speechPoint.getCurrrentPoints();
            cashoutSpeechPoints = speechPoint.getCashoutPending();
            currentTimeString = speechPoint.getTime();


            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        final Dialog dialog = new Dialog(MainActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.points_details_layout);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);

        final TextView totalSubmitted = (TextView) dialog.findViewById(R.id.totalSubmitted);
        final TextView totalPoints = (TextView) dialog.findViewById(R.id.totalPoints);
        final TextView currentPoints = (TextView) dialog.findViewById(R.id.currentPoints);
        final TextView cashoutReq = (TextView) dialog.findViewById(R.id.cashoutReq);
        final TextView title = (TextView) dialog.findViewById(R.id.title);
        final TextView currentTime = (TextView) dialog.findViewById(R.id.currentTime);
        final LinearLayout pointDetailsLayout = (LinearLayout) dialog.findViewById(R.id.pointDetailsLayout);
        pointDetailsLayout.setVisibility(View.VISIBLE);
        AppCompatButton cancelBtn = (AppCompatButton) dialog.findViewById(R.id.cancelBtn);
        AppCompatButton refresh = (AppCompatButton) dialog.findViewById(R.id.refresh);
        LinearLayout cancel_layout = (LinearLayout) dialog.findViewById(R.id.cancel_layout);
        AppCompatButton cashoutReqBtn = (AppCompatButton) dialog.findViewById(R.id.cashoutReqBtn);
        cashoutReqBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (Double.parseDouble(currentSpeechPoints) <10){
                    new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Sorry!!!")
                            .setContentText("You can not request at the bottom of 10 points")
                            .setConfirmText("Try Again")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();

                                }
                            })
                            .show();
                    return;
                }

                dialog.dismiss();
                pointRequest(Constant.point_speech_request, "Speech");
            }
        });
        cancel_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                points(Constant.point_speech);

                final SweetAlertDialog pogressDialiog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                pogressDialiog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
                pogressDialiog.setTitleText("Loading");
                pogressDialiog.setCancelable(false);
                pogressDialiog.show();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pogressDialiog.dismiss();
                        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Success!!!")
                                .setContentText("Points have been updated!")
                                .setConfirmText("Oky")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                                        sweetAlertDialog.dismiss();

                                    }
                                })
                                .show();
                        try {
                            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

                            Points speechPoint = new Points();
                            speechPoint = snappydb.get(Constant.POINTS_SPEECH_KEY, Points.class);
                            totalSpeechPoints = speechPoint.getTotalPoints();
                            totalSpeeechSubmitted = speechPoint.getTotalSubmitted();
                            currentSpeechPoints = speechPoint.getCurrrentPoints();
                            cashoutSpeechPoints = speechPoint.getCashoutPending();
                            currentTimeString = speechPoint.getTime();

                            snappydb.close();


                            title.setText("Speech");
                            currentTime.setText(currentTimeString);
                            totalSubmitted.setText(totalSpeeechSubmitted);
                            totalPoints.setText(totalSpeechPoints);
                            currentPoints.setText(currentSpeechPoints);
                            cashoutReq.setText(cashoutSpeechPoints);
                            SharedPreferences prefs = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE);
                            String totalSubmitted = prefs.getString("totalSubmitted", "0");


                            speech_current_points.setText(totalSubmitted);
                            mSwipeRefreshLayout.setRefreshing(false);

                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }


                    }
                }, 6000);

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        title.setText("Speech");
        totalSubmitted.setText(totalSpeeechSubmitted);
        totalPoints.setText(totalSpeechPoints);
        currentTime.setText(currentTimeString);
        currentPoints.setText(currentSpeechPoints);
        cashoutReq.setText(cashoutSpeechPoints);

//        recycler_view =  (RecyclerView) dialog.findViewById(R.id.recycler_view);
//        no_content_tv = (TextView) dialog.findViewById(R.id.no_content_tv);


        dialog.show();

    }

    private void speechPointsDetails1() {

        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name
            Points speechPoint = new Points();
            speechPoint = snappydb.get(Constant.POINTS_SPEECH_KEY, Points.class);
            totalSpeechPoints = speechPoint.getTotalPoints();
            totalSpeeechSubmitted = speechPoint.getTotalSubmitted();
            currentSpeechPoints = speechPoint.getCurrrentPoints();
            cashoutSpeechPoints = speechPoint.getCashoutPending();

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        points(Constant.point_speech);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                try {
                    snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

                    Points speechPoint = new Points();
                    speechPoint = snappydb.get(Constant.POINTS_SPEECH_KEY, Points.class);
                    totalSpeechPoints = speechPoint.getTotalPoints();
                    totalSpeeechSubmitted = speechPoint.getTotalSubmitted();
                    currentSpeechPoints = speechPoint.getCurrrentPoints();
                    cashoutSpeechPoints = speechPoint.getCashoutPending();
                    SharedPreferences prefs = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE);
                    String totalSubmitted = prefs.getString("totalSubmitted", "0");

                    speech_current_points.setText(totalSubmitted);
//                    Toast.makeText(MainActivity.this, "totalSpeeechSubmitted"+totalSpeeechSubmitted, Toast.LENGTH_SHORT).show();

                    snappydb.close();
                    mSwipeRefreshLayout.setRefreshing(false);

                } catch (SnappydbException e) {
                    e.printStackTrace();
                }


            }
        }, 3000);
    }

    private void pointRequest(final String endPOINTS, String titleString) {
        String token = null;
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            token = model.getToken();


            Log.i(TAG, "TOKEN:" + token);

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }


        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.point_cashout_request);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);

        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setText("Cashout Request For " + titleString);


        RadioGroup radio_group = (RadioGroup) dialog.findViewById(R.id.radio_group);
        final RadioButton radio_bkash = (RadioButton) dialog.findViewById(R.id.radio_bkash);
        final RadioButton radio_rocket = (RadioButton) dialog.findViewById(R.id.radio_rocket);

        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == radio_bkash.getId()) {
                    method = "bkash";
                } else if (i == radio_rocket.getId()) {
                    method = "rocket";
                }
            }
        });
        ;
        final TextInputEditText points = (TextInputEditText) dialog.findViewById(R.id.points);
        Button send_btn = (Button) dialog.findViewById(R.id.send_btn);


        final String finalToken = token;
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final SweetAlertDialog pogressDialiog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                pogressDialiog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimaryDark));
                pogressDialiog.setTitleText("Loading");
                pogressDialiog.setCancelable(false);
                pogressDialiog.show();

                if (!points.getText().toString().equals("")) {
                    if (endPOINTS.equals(Constant.point_speech_request)) {
                        if (Double.valueOf(points.getText().toString()) > Double.valueOf(currentSpeechPoints)) {
                            points.setError("Enter validate number");
                            pogressDialiog.dismiss();
                            return;
                        }
                    } else {
                        if (Double.valueOf(points.getText().toString()) > Double.valueOf(currentSentimentPoints)) {
                            points.setError("Enter validate number");
                            pogressDialiog.dismiss();
                            return;
                        }
                    }
                } else {
                    points.setError("Enter validate number");
                    pogressDialiog.dismiss();
                    return;
                }

                if (Double.parseDouble(points.getText().toString()) <10){

                    pogressDialiog.dismiss();
                    new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Sorry!!!")
                            .setContentText("You can not request at the bottom of 10 points")
                            .setConfirmText("Try Again")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();

                                }
                            })
                            .show();
                    return;
                }


                RequestParams params = new RequestParams();
                params.put("points_request", points.getText().toString());
                params.put("pay_by", method);


                AsyncHttpClient client = new AsyncHttpClient();
                client.post(Constant.BASE_URL + endPOINTS + "?api_token=" + finalToken, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                        try {
                            if (response.getString("code").equals("200")) {

                                points(Constant.point_speech);
                                points(Constant.point_sentiment);

                                if (endPOINTS.equals(Constant.point_sentiment_request)) {
                                    points(Constant.point_sentiment);
                                } else if (endPOINTS.equals(Constant.point_speech_request)) {
                                    points(Constant.point_speech);
                                }
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        pogressDialiog.dismiss();
                                        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Congratulations!!!")
                                                .setContentText("You have been requested for cashout")
                                                .setConfirmText("OK")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                        dialog.dismiss();

                                                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                                        sweetAlertDialog.dismiss();
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                })
                                                .show();


                                    }
                                }, 3000);


                            } else if (response.getString("code").equals("405")) {

                                pogressDialiog.dismiss();
                                new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Opss!!!")
                                        .setContentText("Token Mismatched")
                                        .setConfirmText("Log in again")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismiss();
                                                logout();
                                            }
                                        })
                                        .show();
                            } else {

                                //failed

                                pogressDialiog.dismiss();
                                new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Opss!!!")
                                        .setContentText("Sorry, something went wrong.")
                                        .setConfirmText("try again")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismiss();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        pogressDialiog.dismiss();
                        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Sorry, something went wrong.")
                                .setConfirmText("try again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        //failed
                        pogressDialiog.dismiss();
                        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Opss!!!")
                                .setContentText("Sorry, something went wrong.")
                                .setConfirmText("try again")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();

                    }
                });


            }
        });

        dialog.show();


    }

    public void startRepeatingTimer() {

        Context context = this.getApplicationContext();

        if (alarm != null) {

            alarm.SetAlarm(context);

        } else {

            Toast.makeText(context, "data is not available for this moment,contact us!", Toast.LENGTH_SHORT).show();

        }
    }


    public void cancelRepeatingTimer() {
        Context context = this.getApplicationContext();

        if (alarm != null) {

            alarm.CancelAlarm(context);

        } else {

            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();

        }

    }

    public void onetimeTimer(View view) {

        Context context = this.getApplicationContext();

        if (alarm != null) {

            alarm.setOnetimeTimer(context);

        } else {

            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();

        }

    }

    private void setUserName() {
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            String name = model.getName();

            if (!name.isEmpty()) {
                user_name.setText(name + "");

            }
//            Log.i(TAG, "TOKEN:" + token);

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }


}


