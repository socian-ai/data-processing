package ai.socian.dataprocessing.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ai.socian.dataprocessing.activity.MainActivity;
import cz.msebera.android.httpclient.Header;
import ai.socian.dataprocessing.activity.LoginActivity;
import ai.socian.dataprocessing.model.Points;
import ai.socian.dataprocessing.model.SentimentContent;
import ai.socian.dataprocessing.model.SpeechContent;
import ai.socian.dataprocessing.model.User;
import ai.socian.dataprocessing.services.GetIfSpeechContentAvailable;

/**
 * Created by tamzid on 1/26/18.
 */

public class CommonActivity extends AppCompatActivity {

    public static String TAG = "ActivityTAG";
    SharedPreferencesData sharedPreferencesData;
    List<SpeechContent> speechList;
    List<SentimentContent> sentimentList;
    List<SentimentContent> sentimentEmotionList;
    User userModel;
    DB snappydb;
    private GetIfSpeechContentAvailable alarm;

    public void logout() {
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name
            snappydb.del(Constant.USER_MODEL);
            SharedPreferencesData sharedPreferencesData = new SharedPreferencesData();
            sharedPreferencesData.clearDataSharedPref(Constant.SPEECH_CONTENT);
            sharedPreferencesData.clearDataSharedPref(Constant.SENTIMENT_CONTENT);

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        alarm = new GetIfSpeechContentAvailable();
        cancelRepeatingTimer();

        Intent intent = new Intent(ApplicationSingleton.getContext(), LoginActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ApplicationSingleton.getContext().startActivity(intent);
        finish();
    }

    public void cancelRepeatingTimer() {
        Context context = ApplicationSingleton.getContext();

        if (alarm != null) {

            alarm.CancelAlarm(context);

        } else {

            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();

        }

    }

    public void loadDataIfnotAvailable() {

        List<SpeechContent> speechContentList = new ArrayList<>();
        sharedPreferencesData = new SharedPreferencesData();
        speechContentList = getSpeechListStorageData();

        if (speechContentList == null || speechContentList.size() <= 25) {
            speechContentList = apiCallSpeech();
        } else {

            //nothing to do
        }
//        if (sentimentList == null || sentimentList.size() <= 10) {
//            sentimentList = apiCallSentiment();
//        } else {
//
//            //nothing to do
//        }


        List<SentimentContent> sentimentContents = new ArrayList<>();
        sharedPreferencesData = new SharedPreferencesData();
        sentimentContents = getSentimentListStorageData();

        if (sentimentContents == null || sentimentContents.size() <= 25) {
            sentimentContents = apiCallSentiment();
        } else {

            //nothing to do
        }

        List<SentimentContent> sentimentEmotionContents = new ArrayList<>();
        sharedPreferencesData = new SharedPreferencesData();
        sentimentEmotionContents = getSentimentEmotionListStorageData();

        if (sentimentEmotionContents == null || sentimentEmotionContents.size() <= 25) {
            sentimentEmotionContents = apiCallSentimentEmotion();
        } else {

            //nothing to do
        }

    }

    public List<SpeechContent> apiCallSpeech() {

        speechList = new ArrayList<SpeechContent>();
        sharedPreferencesData = new SharedPreferencesData();

        String token = null;
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            token = model.getToken();


            Log.i(TAG, "TOKEN:" + token);

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("api_token", token);
        client.get(Constant.speech, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                try {
                    if (response.getString("code").equals("200")) {

                        try {
                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);

                                int id = object.getInt("id");
                                int userId = object.getInt("user_id");
                                //                            String token = object.getString("api_token");
                                String content = object.getString("content");
                                int is_Submited = object.getInt("is_Submited");
                                int is_requested = object.getInt("is_requested");


                                SpeechContent modelSpeech = new SpeechContent();
                                modelSpeech.setId(id);
                                modelSpeech.setContent(content);
                                modelSpeech.setIsRequesed(is_requested);
                                modelSpeech.isSubmited(is_Submited);
                                speechList = new ArrayList<SpeechContent>();
                                speechList.add(modelSpeech);

                                sharedPreferencesData.addSharedPrefSpeechContent(modelSpeech, Constant.SPEECH_CONTENT);


                            }

                            Log.i(TAG, "array_data: " + speechList.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    } else if (response.getString("code").equals("405")) {
                        logout();
                        speechList = null;
                    } else {
                        speechList = null;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                speechList = null;
            }
        });

        return speechList;
    }

    public List<SentimentContent> apiCallSentiment() {

        speechList = new ArrayList<SpeechContent>();
        sharedPreferencesData = new SharedPreferencesData();

        String token = null;
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            token = model.getToken();


            Log.i(TAG, "TOKEN:" + token);

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("api_token", token);
        client.get(Constant.sentiment, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                try {
                    if (response.getString("code").equals("200")) {

                        try {
                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);

                                int id = object.getInt("id");
                                int userId = object.getInt("user_id");
                                //                            String token = object.getString("api_token");
                                String content = object.getString("content");
                                int is_Submited = object.getInt("is_Submited");
                                int is_requested = object.getInt("is_requested");


                                SentimentContent modelSpeech = new SentimentContent();
                                modelSpeech.setId(id);
                                modelSpeech.setContent(content);
                                modelSpeech.setIsRequesed(is_requested);
                                modelSpeech.setIsSubmited(is_Submited);
                                sentimentList = new ArrayList<SentimentContent>();
                                sentimentList.add(modelSpeech);

                                sharedPreferencesData.addSharedPrefSentimentContent(modelSpeech, Constant.SENTIMENT_CONTENT);


                            }

                            Log.i(TAG, "array_data: " + speechList.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    } else if (response.getString("code").equals("405")) {
                        logout();
                        sentimentList = null;

                    } else {
                        sentimentList = null;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                sentimentList = null;
            }
        });

        return sentimentList;
    }

    public List<SentimentContent> apiCallSentimentEmotion() {

        sentimentEmotionList = new ArrayList<SentimentContent>();
        sharedPreferencesData = new SharedPreferencesData();

        String token = null;
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            token = model.getToken();


            Log.i(TAG, "TOKEN:" + token);

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("api_token", token);
        client.get(Constant.emotion, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                try {
                    if (response.getString("code").equals("200")) {

                        try {
                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);

                                int id = object.getInt("id");
                                int userId = object.getInt("user_id");
                                //                            String token = object.getString("api_token");
                                String content = object.getString("content");
                                int is_Submited = object.getInt("is_Submited");
                                int is_requested = object.getInt("is_requested");


                                SentimentContent modelSpeech = new SentimentContent();
                                modelSpeech.setId(id);
                                modelSpeech.setContent(content);
                                modelSpeech.setIsRequesed(is_requested);
                                modelSpeech.setIsSubmited(is_Submited);
                                sentimentEmotionList = new ArrayList<SentimentContent>();
                                sentimentEmotionList.add(modelSpeech);

                                sharedPreferencesData.addSharedPrefSentimentContent(modelSpeech, Constant.SENTIMENT_CONTENT_EMOTION);


                            }

                            Log.i(TAG, "array_data: " + sentimentEmotionList.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    } else if (response.getString("code").equals("405")) {
                        logout();
                        sentimentEmotionList = null;

                    } else {
                        sentimentEmotionList = null;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                sentimentEmotionList = null;
            }
        });

        return sentimentEmotionList;
    }


    public List<SpeechContent> getSpeechListStorageData() {

        speechList = new ArrayList<SpeechContent>();
        sharedPreferencesData = new SharedPreferencesData();
        speechList = sharedPreferencesData.getSharedPrefSpeechContent(Constant.SPEECH_CONTENT);

        if (speechList == null || speechList.size() < 0) {
            return speechList = null;
        } else {
            return speechList;
        }
    }

    public List<SentimentContent> getSentimentListStorageData() {

        sentimentList = new ArrayList<SentimentContent>();
        sharedPreferencesData = new SharedPreferencesData();
        sentimentList = sharedPreferencesData.getSharedPrefSentimentContent(Constant.SENTIMENT_CONTENT);

        if (sentimentList == null || sentimentList.size() < 0) {
            return sentimentList = null;
        } else {
            return sentimentList;
        }
    }

    public List<SentimentContent> getSentimentEmotionListStorageData() {

        sentimentEmotionList = new ArrayList<SentimentContent>();
        sharedPreferencesData = new SharedPreferencesData();
        sentimentEmotionList = sharedPreferencesData.getSharedPrefSentimentContent(Constant.SENTIMENT_CONTENT_EMOTION);

        if (sentimentEmotionList == null || sentimentEmotionList.size() < 0) {
            return sentimentEmotionList = null;
        } else {
            return sentimentEmotionList;
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        boolean isconnected;
        if (netInfo == null || !netInfo.isConnected())
            isconnected = false;
        else
            isconnected = true;
        Log.v("isOnliNe", isconnected + "");
        return isconnected;
    }


    public void points(final String endPOINTS) {
        final Points points = new Points();

        String token = null;
        try {
            snappydb = DBFactory.open(ApplicationSingleton.getContext()); //create or open an existing database using the default name

            User model = new User();
            model = snappydb.get(Constant.USER_MODEL, User.class);

            token = model.getToken();


            Log.i(TAG, "TOKEN:" + token);

            snappydb.close();

        } catch (SnappydbException e) {
            e.printStackTrace();
        }


        AsyncHttpClient client = new AsyncHttpClient();
        client.get(endPOINTS + "?api_token=" + token, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i(TAG, "Status Code :" + statusCode + "\n Header :" + headers.toString() + "\n responseBody :" + response.toString());

                try {
                    if (response.getString("code").equals("200")) {


                        try {
                            JSONObject user = new JSONObject();
                            user = response;
                            String totalPoints = null;

                            String totalSubmitted = "0";
                            String currentPoints = "0";
                            String cashOutPending = "0";
                            String time = null;

                            if (endPOINTS.equals(Constant.point_speech)) {
                                totalSubmitted = response.getString("Total_submit");

                                try {
                                    SharedPreferences.Editor editor = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE).edit();
                                    editor.putString("totalSubmitted", totalSubmitted);
                                    editor.apply();
                                    Log.i(TAG, "totalSubmittedy:" + points.getTotalSubmitted());

                                    currentPoints = response.getString("Current_point_in_tk");
                                    cashOutPending = response.getString("Cashout_pending");
                                    totalPoints = response.getString("total_point_in_tk");
                                    String timeInSec = response.getString("Current_point");

                                    try {
                                        time = getDurationString(Integer.parseInt(timeInSec));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }catch (Exception e){
                                    e.printStackTrace();
                                }




                            } else if (endPOINTS.equals(Constant.point_sentiment)) {

                                totalSubmitted = response.getString("Total_submit");
                                currentPoints = response.getString("current_point_in_tk");
                                cashOutPending = response.getString("Cashout_pending");
                                totalPoints = response.getString("total_point_in_tk");
                                time = "0";
                            } else {
                                try {
                                    totalSubmitted = response.getString("Total_submit");
                                    currentPoints = response.getString("current_point_in_tk");
                                    cashOutPending = response.getString("Cashout_pending");
                                    totalPoints = response.getString("total_point_in_tk");
                                    time = "0";
                                    Log.i(TAG, "totalSubmittedx:" + totalSubmitted);

                                    SharedPreferences.Editor editor = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE).edit();
                                    editor.putString("totalSubmitted", totalSubmitted);
                                    editor.apply();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            points.setTotalSubmitted(totalSubmitted);
                            points.setTotalPoints(totalPoints);
                            points.setCurrrentPoints(currentPoints);
                            points.setCashoutPending(cashOutPending);
                            points.setTime(time);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                try {
                                    snappydb = DBFactory.open(ApplicationSingleton.getContext());
                                    if (endPOINTS.equals(Constant.point_speech)) {
                                        //                            sharedPreferencesData.addSharedPrefPoints(points,Constant.POINTS_SPEECH_KEY);
                                        Log.i(TAG, "totalSubmitted1:" + points.getTotalSubmitted());



                                        snappydb.put(Constant.POINTS_SPEECH_KEY, points);



                                    } else if (endPOINTS.equals(Constant.point_sentiment)) {
                                        snappydb.put(Constant.POINTS_SENTIMENT_KEY, points);

                                    } else {
                                        snappydb.put(Constant.POINTS_SPEECH_KEY, points);
                                        Log.i(TAG, "totalSubmitted2:" + points.getTotalSubmitted());

                                    }

                                    snappydb.close();
                                    //success
                                } catch (SnappydbException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, 1000);


                    } else if (response.getString("code").equals("405")) {
                        logout();
                        //failed
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                //failed
            }
        });

    }

    private String getDurationString(int seconds) {

        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;

        return twoDigitString(hours) + " : " + twoDigitString(minutes) + " : " + twoDigitString(seconds);
    }

    private String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

}
