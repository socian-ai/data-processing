package ai.socian.dataprocessing.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

/**
 * Created by tamzi on 10/4/2017.
 */

public class PermissionUtility {

    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;

        public boolean checkAndRequestPermissions(final Activity context) {

             String permissions = Manifest.permission.RECORD_AUDIO;

            permissionCheck(context,permissions,REQUEST_RECORD_AUDIO_PERMISSION);

            return true;
        }

        public void permissionCheck(Activity context , String Permissions, int requestCode){

            if (ContextCompat.checkSelfPermission(context,
                    Permissions)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                        Permissions)) {

                } else {

                    ActivityCompat.requestPermissions(context,
                            new String[]{Permissions},
                            requestCode);
                }
            }

        }

        public static void showDialogOK(final Activity context, String message) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage(message);
            alertBuilder.setPositiveButton(android.R.string.yes,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
//                            PermissionUtility.checkAndRequestPermissions(context);
                        }
                    });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        }


    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 130;

    public static boolean checkPermission(final Context context) {

        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Camera permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[] { Manifest.permission.CAMERA }, MY_PERMISSIONS_REQUEST_CAMERA);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                            MY_PERMISSIONS_REQUEST_CAMERA);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}