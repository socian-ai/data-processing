package ai.socian.dataprocessing.util;

/**
 * Created by tamzid on 1/24/18.
 */

public class Constant {

//    public static  final String  BASE_URL = "http://socian-data-processing.southeastasia.cloudapp.azure.com/socian_data_processing/socian_data_processing/public/";
    public static  final String  BASE_URL = "https://socian.ai/socian_data_process/public/";

    public static final String login = BASE_URL+"login";
//    public static final String point_speech = BASE_URL+"point";
    public static final String point_speech = BASE_URL+"emotion_point";
    public static final String point_sentiment = BASE_URL+"sentiment_point";
    public static final String point_speech_request = "point_request_post";
    public static final String point_sentiment_request = "sentiment_point_req_post";
    public static final String register = BASE_URL+"register";
    public static final String forgot_password = BASE_URL+"forgot_password";
    public static final String password_updated = BASE_URL+"password_updated";
    public static final String number_verification = BASE_URL+"number_verification";
    public static final String speech = BASE_URL+"speech";
    public static final String sentiment = BASE_URL+"sentiment";
    public static final String emotion = BASE_URL+"emotion";
    public static final String speech_update = BASE_URL+"speech_update/";
    public static final String sentiment_update = BASE_URL+"sentiment_update/";
    public static final String emotion_update = BASE_URL+"emotion_update/";
    public static final String profile_update = BASE_URL+"profile_update";
    public static final String USER_MODEL = "USER";
    public static final String SPEECH_CONTENT ="speech_content";
    public static final String SENTIMENT_CONTENT ="sentiment_content";
    public static final String SENTIMENT_CONTENT_EMOTION ="sentiment_content_emotion";
    public static final String POINTS_SENTIMENT_KEY ="POINTS_SENTIMENT_KEY";
    public static final String POINTS_SPEECH_KEY ="POINTS_SPEECH_KEY";

    public static final String SPEECH_POSITION ="Position";


}
