package ai.socian.dataprocessing.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ai.socian.dataprocessing.model.Points;
import ai.socian.dataprocessing.model.SentimentContent;
import ai.socian.dataprocessing.model.SpeechContent;

/**
 * Created by tamzid on 2/14/17.
 */

public class SharedPreferencesData {


    public void clearDataSharedPref(String MY_PREFS_NAME) {
        SharedPreferences preferences = ApplicationSingleton.getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    public void saveList() {


    }



    //// to get speech content

    public List<SpeechContent> getSharedPrefSpeechContent(String MY_PREFS_NAME) {
        Gson gson = new Gson();
        List<SpeechContent> data = new ArrayList<>();
        SharedPreferences sharedPref = ApplicationSingleton.getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        String jsonPreferences = sharedPref.getString("key", "");

        Type type = new TypeToken<List<SpeechContent>>() {
        }.getType();
        data = gson.fromJson(jsonPreferences, type);

        return data;
    }

    // to add speech content

    public void addSharedPrefSpeechContent(SpeechContent addData, String MY_PREFS_NAME) {

        Gson gson = new Gson();
        SharedPreferences sharedPref = ApplicationSingleton.getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);

        String jsonSaved = sharedPref.getString("key", "");
        String jsonNewproductToAdd = gson.toJson(addData);

        JSONArray jsonArrayProduct = new JSONArray();

        try {
            if (jsonSaved.length() != 0) {
                jsonArrayProduct = new JSONArray(jsonSaved);
            }
            jsonArrayProduct.put(new JSONObject(jsonNewproductToAdd));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //SAVE NEW ARRAY
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("key", String.valueOf(jsonArrayProduct));
        editor.commit();
    }




    //// to get sentiment content

    public List<SentimentContent> getSharedPrefSentimentContent(String MY_PREFS_NAME) {
        Gson gson = new Gson();
        List<SentimentContent> data = new ArrayList<>();
        SharedPreferences sharedPref = ApplicationSingleton.getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        String jsonPreferences = sharedPref.getString("key", "");

        Type type = new TypeToken<List<SentimentContent>>() {
        }.getType();
        data = gson.fromJson(jsonPreferences, type);

        return data;
    }

    // to add speech content

    public void addSharedPrefSentimentContent(SentimentContent addData, String MY_PREFS_NAME) {

        Gson gson = new Gson();
        SharedPreferences sharedPref = ApplicationSingleton.getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);

        String jsonSaved = sharedPref.getString("key", "");
        String jsonNewproductToAdd = gson.toJson(addData);

        JSONArray jsonArrayProduct = new JSONArray();

        try {
            if (jsonSaved.length() != 0) {
                jsonArrayProduct = new JSONArray(jsonSaved);
            }
            jsonArrayProduct.put(new JSONObject(jsonNewproductToAdd));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //SAVE NEW ARRAY
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("key", String.valueOf(jsonArrayProduct));
        editor.commit();
    }


    //// to get points

    public List<Points> getSharedPrefPoints(String MY_PREFS_NAME) {
        Gson gson = new Gson();
        List<Points> data = new ArrayList<>();
        SharedPreferences sharedPref = ApplicationSingleton.getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        String jsonPreferences = sharedPref.getString("key", "");

        Type type = new TypeToken<List<Points>>() {
        }.getType();
        data = gson.fromJson(jsonPreferences, type);

        return data;
    }

    // to add points

    public void addSharedPrefPoints(Points addData, String MY_PREFS_NAME) {

        Gson gson = new Gson();
        SharedPreferences sharedPref = ApplicationSingleton.getContext().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);

        String jsonSaved = sharedPref.getString("key", "");
        String jsonNewproductToAdd = gson.toJson(addData);

        JSONArray jsonArrayProduct = new JSONArray();

        try {
            if (jsonSaved.length() != 0) {
                jsonArrayProduct = new JSONArray(jsonSaved);
            }
            jsonArrayProduct.put(new JSONObject(jsonNewproductToAdd));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //SAVE NEW ARRAY
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("key", String.valueOf(jsonArrayProduct));
        editor.commit();
    }


    public void addString(String addData,String KEY) {


        SharedPreferences sharedpreferences = ApplicationSingleton.getContext().getSharedPreferences(KEY, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString(KEY, addData);
        editor.commit();

    }

    public String getString(String KEY) {

        SharedPreferences sharedPref = ApplicationSingleton.getContext().getSharedPreferences(KEY, Context.MODE_PRIVATE);
        String data = sharedPref.getString(KEY, "");


        return data;
    }

}