package ai.socian.dataprocessing.model;

import java.io.Serializable;

/**
 * Created by tamzid on 2/7/18.
 */

public class Points implements Serializable {

    private String totalPoints;
    private String totalSubmitted;
    private String currrentPoints;
    private String CashoutPending;
    private String time;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getTotalSubmitted() {
        return totalSubmitted;
    }

    public void setTotalSubmitted(String totalSubmitted) {
        this.totalSubmitted = totalSubmitted;
    }

    public String getCurrrentPoints() {
        return currrentPoints;
    }

    public void setCurrrentPoints(String currrentPoints) {
        this.currrentPoints = currrentPoints;
    }

    public String getCashoutPending() {
        return CashoutPending;
    }

    public void setCashoutPending(String cashoutPending) {
        CashoutPending = cashoutPending;
    }
}

