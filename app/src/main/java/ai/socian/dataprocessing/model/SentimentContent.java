package ai.socian.dataprocessing.model;

import java.io.Serializable;

/**
 * Created by tamzid on 2/5/18.
 */

public class SentimentContent implements Serializable{

    private int id;
    private String content;
    private int isSubmited;
    private boolean positive;
    private boolean negative;
    private boolean neutral;
    private boolean question;
    private boolean slang;
    private boolean other;
    private int isRequesed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getIsSubmited() {
        return isSubmited;
    }

    public void setIsSubmited(int isSubmited) {
        this.isSubmited = isSubmited;
    }

    public boolean isPositive() {
        return positive;
    }

    public void setPositive(boolean positive) {
        this.positive = positive;
    }

    public boolean isNegative() {
        return negative;
    }

    public void setNegative(boolean negative) {
        this.negative = negative;
    }

    public boolean isNeutral() {
        return neutral;
    }

    public void setNeutral(boolean neutral) {
        this.neutral = neutral;
    }

    public boolean isQuestion() {
        return question;
    }

    public void setQuestion(boolean question) {
        this.question = question;
    }

    public boolean isSlang() {
        return slang;
    }

    public void setSlang(boolean slang) {
        this.slang = slang;
    }

    public boolean isOther() {
        return other;
    }

    public void setOther(boolean other) {
        this.other = other;
    }

    public int getIsRequesed() {
        return isRequesed;
    }

    public void setIsRequesed(int isRequesed) {
        this.isRequesed = isRequesed;
    }
}
