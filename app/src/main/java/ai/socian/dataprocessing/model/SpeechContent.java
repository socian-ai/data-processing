package ai.socian.dataprocessing.model;

import java.io.Serializable;

/**
 * Created by tamzid on 1/24/18.
 */

public class SpeechContent implements Serializable{
    private int id;
    private String content;
    private int isSubmited;
    private String AudioPath;
    private String submitedBy;
    private int isRequesed;
    private String filePath;


    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getIsSubmited() {
        return isSubmited;
    }

    public void setIsSubmited(int isSubmited) {
        this.isSubmited = isSubmited;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int isSubmited(int is_requested) {
        return isSubmited;
    }

    public void setSubmited(int submited) {
        isSubmited = submited;
    }

    public String getAudioPath() {
        return AudioPath;
    }

    public void setAudioPath(String audioPath) {
        AudioPath = audioPath;
    }

    public String getSubmitedBy() {
        return submitedBy;
    }

    public void setSubmitedBy(String submitedBy) {
        this.submitedBy = submitedBy;
    }

    public int getIsRequesed() {
        return isRequesed;
    }

    public void setIsRequesed(int isRequesed) {
        this.isRequesed = isRequesed;
    }

}
